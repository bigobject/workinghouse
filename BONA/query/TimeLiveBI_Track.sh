#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin


###CHANGE if you have special requirement
ShareVariables=0 #set 1 to get output variables from TimeLiveBI
ResultExclude="	0" #the result will filter out whatever you set (string)
UseCache=0 #set 1 to create cache to increase loading speed
REMOTECLUSTER="cluster" #set cluster or remote select
PICK_TRACKER=1 #1: pick tracker to create track group  2: set up advanced track
OUTPUTTABLE="Track" #output table name
split_symbol="⋂" #split symbol for each level
BOIP=${BO%%:*}
BOPORT=${BO#*:}

start=$(date +'%s')


if [[ $CONDITIONS == "intersection" ]] || [[ $CONDITIONS == "union" ]]; then
	PICK_TRACKER=2
	/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "$OUTPUTTABLE" -PARTITION_DB "$PARTITION_DB" -PARTITION_PUSHDOWNDB "$PARTITION_PUSHDOWNDB" -PARTITION_TIME "$PARTITION_TIME" -TABLE "$TABLE_PARAM" -X "$X_PARAMS" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "$FILTER_PARAMS" -TRACK "$TRACK_TICKET" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"
else
	# echo "$TABLE_PARAM" -ip bigobject
	/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "$OUTPUTTABLE" -PARTITION_DB "$PARTITION_DB" -PARTITION_PUSHDOWNDB "$PARTITION_PUSHDOWNDB" -PARTITION_TIME "$PARTITION_TIME" -TABLE "$TABLE_PARAM" -X "$X_PARAMS" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "$FILTER_PARAMS" -TRACK "$TRACK_TICKET" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"
fi


echo "It took $(($(date +'%s') - $start)) seconds"
