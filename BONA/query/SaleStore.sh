#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

OUTPUT_TABLE="SaleStore_${POSTFIX}"
echo "${DATE}"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area))"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area))"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area))"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area))"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1  from (select store_id,sum(qty) as qty,sum(trans_price) as renvenue  from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1  from (select store_id,sum(qty) as qty,sum(trans_price) as renvenue  from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
	fi
		
fi
