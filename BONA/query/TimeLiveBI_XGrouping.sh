#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin


###CHANGE if you have special requirement
# OUTPUTTABLE="XGroup" #output table name
BOIP=${BO%%:*}
BOPORT=${BO#*:}

start=$(date +'%s')
# echo -e "${XGROUP}" | od -c	

mysql -h $BOIP -e "drop table ${OUTPUTTABLE}_${POSTFIX}" || true

XDIM=${XDIM//\'/\"};

#判斷之後要用哪個欄位join
if [[ $XDIM == *"GROUPKEY:"* ]]; then
	JOINKEY=${XDIM//like /};
	IFS='|' read -a XJOIN <<< ${XDIM//GROUPKEY:/|}; 
	JOINKEY=${XJOIN[1]}	
	JOINKEY=${JOINKEY// /}	
	# if [[ ${XJOIN[0]} != *"JOIN:"* ]]; then
	# 	JOINKEY="a.\`${JOINKEY//,/\`,a.\`}\`"
	# fi
elif [[ $XDIM == *"JOIN:"* ]]; then
	IFS='|' read -a XJOIN <<< ${XDIM//JOIN:/|};
	JOINKEY=${XJOIN[0]}
	JOINKEY=${JOINKEY//like /};
	JOINKEY=${JOINKEY// /}	
else
	JOINKEY=${XDIM//like /};
	JOINKEY="${JOINKEY// /}" #"a.\`${JOINKEY// /}\`"
fi

#判斷是否為datetime
if [[ $JOINKEY == *"datetime("* ]]; then
	FINDDATETIME=${JOINKEY#*"datetime("}
	FINDDATETIME=,${FINDDATETIME%%")"*}
elif [[ $JOINKEY == *"month("* ]]; then
	FINDDATETIME=${JOINKEY#*"month("}
	FINDDATETIME=,${FINDDATETIME%%")"*}
elif [[ $JOINKEY == *"weekday("* ]]; then
	FINDDATETIME=${JOINKEY#*"weekday("}
	FINDDATETIME=,${FINDDATETIME%%")"*}
elif [[ $JOINKEY == *"hour("* ]]; then
	FINDDATETIME=${JOINKEY#*"hour("}
	FINDDATETIME=,${FINDDATETIME%%")"*}
elif [[ $JOINKEY == *"minute("* ]]; then
	FINDDATETIME=${JOINKEY#*"minute("}
	FINDDATETIME=,${FINDDATETIME%%")"*}
elif [[ $JOINKEY == *"second("* ]]; then
	FINDDATETIME=${JOINKEY#*"second("}
	FINDDATETIME=,${FINDDATETIME%%")"*}
elif [[ $JOINKEY == *"cast("* ]] && [[ ${JOINKEY,,} == *"date32"* ]]; then
	FINDDATETIME=${JOINKEY#*"cast("}
	FINDDATETIME=,${FINDDATETIME/asdate*/}
	JOINKEY="CAST (${FINDDATETIME:1} AS DATE32)"
elif  [[ $JOINKEY == *"cast("* ]] && [[ ${JOINKEY,,} == *"date64"* ]]; then
	FINDDATETIME=${JOINKEY#*"cast("}
	FINDDATETIME=,${FINDDATETIME/asdate*/}
	JOINKEY="CAST (${FINDDATETIME:1} AS DATE64)"
fi


#判斷XDIM data type為數字還文字
echo "select group_concat(x_type,',') as schema from (select concat(\"'\",Field,\"' \",Type) as x_type from TimeLiveBISchema_$PRETICKET where xdim = '$XDIM')"
tableschema=`mysql -h $BOIP -s -N -e "select group_concat(x_type,',') as schema from (select concat(\"'\",Field,\"' \",Type) as x_type from TimeLiveBISchema_$PRETICKET where xdim = '$XDIM')"`
echo "tableschema  $tableschema"
	
datatype=`mysql -h $BOIP -s -N -e "select Type from TimeLiveBISchema_$PRETICKET where xdim = '${XDIM}' and Field = 'x'"`
delimiter=" 	 " #" \t "
check="$XGROUP$delimiter" 
while [[ ${check} ]]; do
	GROUP+=( "${check%%"$delimiter"*}" );
    check=${check#*"$delimiter"};
done


if [[ ${datatype^^} == *"INT"* ]] || [[ ${datatype^^} == *"FLOAT"* ]] || [[ ${datatype^^} == *"DOUBLE"* ]]; then
	echo "create table ${OUTPUTTABLE}_${POSTFIX} ('min' $datatype,'max' $datatype,'tag' string, 'joinkey' string, 'type' string)"
	mysql -h $BOIP -e "create table ${OUTPUTTABLE}_${POSTFIX} ('min' $datatype,'max' $datatype,'tag' string, 'joinkey' string, 'type' string)"

	for (( i=0; i< ${#GROUP[@]}; i++ ))
	do
		delimiter=" 		 " #" \t\t "
		TAG=${GROUP[$i]%%"$delimiter"*}
		MINMAX=${GROUP[$i]#*"$delimiter"} 
		# echo -e "${MINMAX}" | od -c	
		
		delimiter=" 			 " #" \t\t\t "
		MIN=${MINMAX%%"$delimiter"*}
		MAX=${MINMAX#*"$delimiter"} 
		if [[ $MIN != "" ]] && [[ $MAX != "" ]];then
			echo "insert into ${OUTPUTTABLE}_${POSTFIX} values ($MIN,$MAX,'$TAG', '${JOINKEY//\"/\\\"}' ,'number')"
			mysql -h $BOIP -e "insert into ${OUTPUTTABLE}_${POSTFIX} values ($MIN,$MAX,'$TAG', '${JOINKEY//\"/\\\"}' ,'number')"	
		elif [[ $MIN != "" ]] && [[ $MAX == "" ]];then
			echo "insert into ${OUTPUTTABLE}_${POSTFIX} values ($MIN,2147483647,'$TAG', '${JOINKEY//\"/\\\"}' ,'number')"
			mysql -h $BOIP -e "insert into ${OUTPUTTABLE}_${POSTFIX} values ($MIN,2147483647,'$TAG', '${JOINKEY//\"/\\\"}' ,'number')"
		elif [[ $MIN == "" ]] && [[ $MAX != "" ]];then
			echo "insert into ${OUTPUTTABLE}_${POSTFIX} values (-2147483648,$MAX,'$TAG', '${JOINKEY//\"/\\\"}' ,'number')"
			mysql -h $BOIP -e "insert into ${OUTPUTTABLE}_${POSTFIX} values (-2147483648,$MAX,'$TAG', '${JOINKEY//\"/\\\"}' ,'number')"
		fi

	done
else
	# echo "create table ${OUTPUTTABLE}_${POSTFIX} ($tableschema,'tag' string, 'joinkey' string, 'type' string)"
	# mysql -h $BOIP -e "create table ${OUTPUTTABLE}_${POSTFIX} ($tableschema,'tag' string, 'joinkey' string, 'type' string)"

	for (( i=0; i< ${#GROUP[@]}; i++ ))
	do

		delimiter=" 		 " #" \t\t "
		TAG=${GROUP[$i]%%"$delimiter"*}
		TAG_ITEMS=${GROUP[$i]#*"$delimiter"} 		
		
		delimiter=" 			 " #" \t\t\t "
		check="${TAG_ITEMS}${delimiter}" 
		ITEMS=()
		while [[ ${check} ]]; do
		    ITEMS+=( "${check%%"$delimiter"*}" );
		    check=${check#*"$delimiter"};
		done

		for (( j=0; j< ${#ITEMS[@]}; j++ ))
		do
			if [[ $XDIM == *"GROUPKEY:"* ]]; then
				echo "insert into ${OUTPUTTABLE}_${POSTFIX} select x,$JOINKEY $FINDDATETIME,cast('$TAG' as string), '${JOINKEY//\"/\\\"}' ,'string' from TimeLiveBI_${PRETICKET} a where x = \"${ITEMS[$j]}\""
				mysql -h $BOIP -e "insert into ${OUTPUTTABLE}_${POSTFIX} select x,$JOINKEY $FINDDATETIME,cast('$TAG' as string), '${JOINKEY//\"/\\\"}' ,'string' from TimeLiveBI_${PRETICKET} a where x = \"${ITEMS[$j]}\"" || mysql -h $BOIP -e "create table ${OUTPUTTABLE}_${POSTFIX} as (select x,$JOINKEY $FINDDATETIME,cast('$TAG' as string) as tag, '${JOINKEY//\"/\\\"}' as joinkey,'string' as type from TimeLiveBI_${PRETICKET} a where x = \"${ITEMS[$j]}\")"
			else
				echo "insert into ${OUTPUTTABLE}_${POSTFIX} select x,cast('$TAG' as string), '${JOINKEY//\"/\\\"}' ,'string' from TimeLiveBI_${PRETICKET} a where x = \"${ITEMS[$j]}\""
				mysql -h $BOIP -e "insert into ${OUTPUTTABLE}_${POSTFIX} select x,cast('$TAG' as string), '${JOINKEY//\"/\\\"}' ,'string' from TimeLiveBI_${PRETICKET} a where x = \"${ITEMS[$j]}\""  || mysql -h $BOIP -e "create table ${OUTPUTTABLE}_${POSTFIX} as (select x,cast('$TAG' as string) as tag, '${JOINKEY//\"/\\\"}' as joinkey,'string' as type from TimeLiveBI_${PRETICKET} a where x = \"${ITEMS[$j]}\")"
			fi
			
			
		done
		
	done
fi

echo "It took $(($(date +'%s') - $start)) seconds"
