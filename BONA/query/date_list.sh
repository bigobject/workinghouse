#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"
#MYSQL="mysql -h 127.0.0.1 -P 3306"
OUTPUT_TABLE="date_list"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}
#DATE_FROM='2020-01-01'
#DATE_END='2020-01-31'
#Cluster_Partition=" partition by alias='internal-prod-bo02.storm.mg' and type='master'"
#group by name
echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

echo "create table $OUTPUT_TABLE as (cluster select date_format(trans_date,'%Y%m') as value, date_format(trans_date,'%Y%m') as name,min(trans_date) as start_date,max(trans_date) as end_date from trans_record group by value order by value desc limit 24);"

$MYSQL -e "create table $OUTPUT_TABLE as (cluster select date_format(trans_date,'%Y%m') as value, date_format(trans_date,'%Y%m') as name,min(trans_date) as start_date,max(trans_date) as end_date from trans_record group by value order by value desc limit 24);"
echo "update $OUTPUT_TABLE set value = '000000' where in (select value from $OUTPUT_TABLE order by value desc limit 1);"
$MYSQL -e "update $OUTPUT_TABLE set value = '000000' where in (select value from $OUTPUT_TABLE order by value desc limit 1);"

