#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"
OUTPUT_TABLE="category_heatmap_${POSTFIX}"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select c.store_name as x ,sum(qty)as y ,d.categoryname_2nd as filter,d.categoryid_1nd as filter1 from (select * from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join  product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join  2nd_category  d on b.categoryid_1nd = d.categoryid_1nd and b.categoryid_2nd = d.categoryid_2nd where c.area in ('$FILTER') and d.categoryid_1nd in ('$FILTER1') group by store_id,d.categoryid_1nd,d.categoryid_2nd order by c.area,x desc )"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select c.store_name as x ,sum(qty)as y ,d.categoryname_2nd as filter,d.categoryid_1nd as filter1 from (select * from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join  product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join  2nd_category  d on b.categoryid_1nd = d.categoryid_1nd and b.categoryid_2nd = d.categoryid_2nd where c.area in ('$FILTER') and d.categoryid_1nd in ('$FILTER1') group by store_id,d.categoryid_1nd,d.categoryid_2nd order by c.area,x desc )"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select c.store_name as x ,sum(qty)as y ,d.categoryname_2nd as filter,d.categoryid_1nd as filter1 from (select * from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join 2nd_category d on b.categoryid_1nd = d.categoryid_1nd and b.categoryid_2nd = d.categoryid_2nd where c.area in ('$FILTER') group by store_id,d.categoryid_1nd,d.categoryid_2nd order by c.area,x desc )"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select c.store_name as x ,sum(qty)as y ,d.categoryname_2nd as filter,d.categoryid_1nd as filter1 from (select * from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join 2nd_category d on b.categoryid_1nd = d.categoryid_1nd and b.categoryid_2nd = d.categoryid_2nd where c.area in ('$FILTER') group by store_id,d.categoryid_1nd,d.categoryid_2nd order by c.area,x desc )"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select d.area as x,sum(qty)as y,c.categoryname_2nd as filter ,c.categoryid_1nd as filter1 from (select * from trans_record where trans_date >='${DATE_FROM}'and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join 2nd_category c on b.categoryid_1nd = c.categoryid_1nd and b.categoryid_2nd = c.categoryid_2nd left join store d on a.store_id = d.store_id where c.categoryid_1nd in ('$FILTER1') group by d.area,c.categoryid_1nd,c.categoryid_2nd order by x,c.categoryid_1nd,y desc)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select d.area as x,sum(qty)as y,c.categoryname_2nd as filter ,c.categoryid_1nd as filter1 from (select * from trans_record where trans_date >='${DATE_FROM}'and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join 2nd_category c on b.categoryid_1nd = c.categoryid_1nd and b.categoryid_2nd = c.categoryid_2nd left join store d on a.store_id = d.store_id where c.categoryid_1nd in ('$FILTER1') group by d.area,c.categoryid_1nd,c.categoryid_2nd order by x,c.categoryid_1nd,y desc)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select d.area as x,sum(qty)as y,c.categoryname_2nd as filter,c.categoryid_1nd as filter1,d.area_rank as area_rank from (select * from trans_record where trans_date >='${DATE_FROM}'and trans_date<='${DATE_END}') a left join  product b on a.product_id = b.product_id left join 2nd_category c on b.categoryid_1nd = c.categoryid_1nd and b.categoryid_2nd = c.categoryid_2nd left join store d on a.store_id = d.store_id group by d.area,c.categoryid_1nd,c.categoryid_2nd order by x,y desc)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select d.area as x,sum(qty)as y,c.categoryname_2nd as filter,c.categoryid_1nd as filter1,d.area_rank as area_rank from (select * from trans_record where trans_date >='${DATE_FROM}'and trans_date<='${DATE_END}') a left join  product b on a.product_id = b.product_id left join 2nd_category c on b.categoryid_1nd = c.categoryid_1nd and b.categoryid_2nd = c.categoryid_2nd left join store d on a.store_id = d.store_id group by d.area,c.categoryid_1nd,c.categoryid_2nd order by x,y desc)"
	fi
		
fi
