#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

# APP="TESTNAME"   #put the name of your dashboard here
# CMD="select * from ptx_company where x = \$CITY" #command for querying ex. select * from table or filename.sh or filename.py
# APP="BAR"
# CMD="select c.BusRoute.SubRouteNameZh_tw as x, count(a.PlateNumb) as y, a.City as label from ptx_list where x in ('168','28','281') group by x,label"
# APP="testconfig_bar"
# CMD="cluster select City,count(*),Direction from BusRealTime last 1000 group by City"
BOIP=${BO%%:*}
MYSQL="mysql -h $BOIP"


#prevent sql injection
IFS=';' read -a CMD <<< $CMD;
if [[ ${CMD// /} == *"droptable"* ]]; then
	exit
fi

$MYSQL -e "create database createcmd"|| true

if [[ $CMD == *".sh" ]] || [[ $CMD == *".py" ]]; then
	$MYSQL -e "drop table createcmd.test_cmd_${POSTFIX}"|| true
	$MYSQL -e "create table createcmd.test_cmd_${POSTFIX} as (select '$CMD' as script)"
	cp /webapp/query/$CMD /webapp/query/$APP.sh
else
	
	$MYSQL -e "drop table createcmd.test_cmd_${POSTFIX}"|| true
	$MYSQL -e "create table createcmd.test_cmd_${POSTFIX} as ($CMD)" 
	
	echo "#!/bin/sh
	#this bash script is generated automatically

	set -e

	PATH=\$PATH:/usr/local/bin:/usr/bin:/bin

	OUTPUTTABLE="$APP"

	BOIP=\${BO%%:*}
	MYSQL=\"mysql -h \$BOIP\"

	\$MYSQL -e \"update cluster\"
	\$MYSQL -e \"drop table \${OUTPUTTABLE}_\${POSTFIX}\"|| true

	echo \"create table \${OUTPUTTABLE}_\${POSTFIX} as ($CMD)\"
	\$MYSQL -e \"create table \${OUTPUTTABLE}_\${POSTFIX} as ($CMD)\"
	" > /webapp/query/$APP.sh
	
	chmod a+x /webapp/query/$APP.sh #set permission 

fi
echo "save script to /webapp/query/$APP.sh"