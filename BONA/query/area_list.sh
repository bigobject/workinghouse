#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"
OUTPUT_TABLE="area_list"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}
#Cluster_Partition=" partition by alias='internal-prod-bo02.storm.mg' and type='master'"
#group by name
echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
echo "create table $OUTPUT_TABLE as (cluster select b.area as header,b.area as value  from (select store_id from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') a left join (select store_id,area from store) b on a.store_id = b.store_id group by header order by header asc);"
$MYSQL -e "create table $OUTPUT_TABLE as (cluster select b.area as header,b.area as value  from (select store_id from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') a left join (select store_id,area from store) b on a.store_id = b.store_id group by header order by header asc);" 

