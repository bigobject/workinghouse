#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"
#MYSQL="mysql -h 127.0.0.1 -P 3306"
OUTPUT_TABLE="category_list"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}
#DATE_FROM='2020-01-01'
#DATE_END='2020-01-31'
#Cluster_Partition=" partition by alias='internal-prod-bo02.storm.mg' and type='master'"
#group by name
echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

echo "create table $OUTPUT_TABLE as (cluster select c.categoryid_1nd as header,c.categoryname_1nd as value from (select product_id from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') a left join (select product_id,categoryid_1nd from product) b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd group by  b.categoryid_1nd order by header);"

$MYSQL -e "create table $OUTPUT_TABLE as (cluster select c.categoryid_1nd as header,c.categoryname_1nd as value from (select product_id from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') a left join (select product_id,categoryid_1nd from product) b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd group by  b.categoryid_1nd order by header);"

