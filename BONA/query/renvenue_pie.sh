#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

OUTPUT_TABLE="renvenue_pie_${POSTFIX}"


#DATE_FROM=${DATE%%~*}
#DATE_END=${DATE#*~}

DATE_value=`$MYSQL -s -N -e "select name from date_list where value='${DATE}'"`

Last_month=`$MYSQL -s -N -e "cluster select cast(year_month as int )-1 from trans_record where year_month = '${DATE_value}' group by year_month"`



echo "---------"$OUTPUT_TABLE
echo "${DATE}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster  select b.store_name as store_name,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER') and c.categoryid_1nd in ('$FILTER1')group by store_name)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select b.store_name as store_name,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER') and c.categoryid_1nd in ('$FILTER1')group by store_name)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster  select b.store_name as store_name,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')group by store_name)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select b.store_name as store_name,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')group by store_name)"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster  select b.area as area,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where c.categoryid_1nd in ('$FILTER1') group by area)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select b.area as area,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where c.categoryid_1nd in ('$FILTER1') group by area)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster  select b.area as area,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id group by area)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select b.area as area,sum(trans_price) as renvenue from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id group by area)"
	fi
		
fi
