#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

#DATE="2020-01-01~2020-05-31"
#POSTFIX='test'
#FILTER="台北市"
#FILTER1=""
#SHOWOPTION="bbb"

#MYSQL="mysql -h 127.0.0.1 -P 3306"
tmp_table="SaleStore_table_tmp_$POSTFIX"
OUTPUT_TABLE="SaleStore_table_$POSTFIX"

DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}

#prevent sql injection


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
FILTER="${FILTER//,/\',\'}"
FILTER1="${FILTER1//,/\',\'}"

if [[ $FILTER != '' ]]; then
	x="b.store_name"
	wheregroup="where b.area in ('$FILTER') group by store_id"
	filter="concat(area,'=',x)"
	filter1="concat(area,'=',categoryname_1nd,'=',x)"
else
	x="b.area"
	wheregroup="group by b.area"
	filter="x"
	filter1="concat(area,'=',categoryname_1nd)"
fi

if [[ $FILTER1 != '' ]]; then
	echo "create table $OUTPUT_TABLE as (cluster select x,concat('<a href=\"../../api/v1/link/',$filter1,'a-${POSTFIX}?format=html\" target=\"123\">',format_num(qty,0),'</a>') as qty,concat('<a href=\"../../api/v1/link/',$filter1,'b-${POSTFIX}?format=html\" target=\"123\">',format_num(renvenue,0),'</a>') as renvenue,area,categoryname_1nd,$filter1 as filter from (select $x as x,sum(qty)as qty,sum(renvenue) as renvenue,b.area as area,categoryname_1nd from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue ,categoryname_1nd from (select store_id,product_id,qty,trans_price,bb.categoryid_1nd as categoryid_1nd,cc.categoryname_1nd as categoryname_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join product bb on aa.product_id = bb.product_id left join 1nd_category cc on bb.categoryid_1nd = cc.categoryid_1nd where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id $wheregroup order by b.area))"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster select x,concat('<a href=\"../../api/v1/link/',$filter1,'a-${POSTFIX}?format=html\" target=\"123\">',format_num(qty,0),'</a>') as qty,concat('<a href=\"../../api/v1/link/',$filter1,'b-${POSTFIX}?format=html\" target=\"123\">',format_num(renvenue,0),'</a>') as renvenue,area,categoryname_1nd,$filter1 as filter from (select $x as x,sum(qty)as qty,sum(renvenue) as renvenue,b.area as area,categoryname_1nd from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue ,categoryname_1nd from (select store_id,product_id,qty,trans_price,bb.categoryid_1nd as categoryid_1nd,cc.categoryname_1nd as categoryname_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join product bb on aa.product_id = bb.product_id left join 1nd_category cc on bb.categoryid_1nd = cc.categoryid_1nd where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id $wheregroup order by b.area))"
else

	echo "create table $OUTPUT_TABLE as (cluster select x,concat('<a href=\"../../api/v1/link/',$filter,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',$filter,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,$filter as filter from (select x,format_num(qty,0) as  qty,format_num(renvenue,0) as renvenue,area from (select $x as x,sum(qty) as qty,sum(renvenue) as renvenue,b.area as area  from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id $wheregroup  order by b.area)))"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster select x,concat('<a href=\"../../api/v1/link/',$filter,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',$filter,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,$filter as filter from (select x,format_num(qty,0) as  qty,format_num(renvenue,0) as renvenue,area from (select $x as x,sum(qty) as qty,sum(renvenue) as renvenue,b.area as area  from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id $wheregroup  order by b.area)))"
fi



PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link_log where link_id in (select link_id from bo_data_link_log a inner join bo_data_link b on a.link_id = b.id where b.ticket = '$POSTFIX')"
PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link where ticket = '$POSTFIX'"

if [[ $FILTER == '' ]] && [[ $FILTER1 == '' ]]; then
	psql_x="[\"s1.area JOIN: left join store s1 on a.store_id = s1.store_id\",\"s2.store_name JOIN: left join store s2 on a.store_id = s2.store_id\"]"
elif [[ $FILTER == '' ]] && [[ $FILTER1 != '' ]]; then
	#psql_x="[\"s.area\",\"b7.categoryname_1nd JOIN: left join 1nd_category b7 on a.p.categoryid_1nd = b7.categoryid_1nd\",\"b8.categoryname_2nd JOIN: left join 2nd_category b8 on a.p.categoryid_1nd = b8.categoryid_1nd and a.p.categoryid_2nd = b8.categoryid_2nd\""
	psql_x="[\"s1.area JOIN: left join store s1 on a.store_id = s1.store_id\",\"b7.categoryname_1nd JOIN: left join product p9 on a.product_id = p9.product_id left join 1nd_category b7 on p9.categoryid_1nd = b7.categoryid_1nd\",\"s2.store_name JOIN: left join store s2 on a.store_id = s2.store_id\"]"
elif [[ $FILTER != '' ]] && [[ $FILTER1 == '' ]]; then
	psql_x="[\"s1.area JOIN: left join store s1 on a.store_id = s1.store_id\",\"s2.store_name JOIN: left join store s2 on a.store_id = s2.store_id\",\"b7.categoryname_1nd JOIN: left join product p9 on a.product_id = p9.product_id left join 1nd_category b7 on p9.categoryid_1nd = b7.categoryid_1nd\"]"
elif [[ $FILTER != '' ]] && [[ $FILTER1 != '' ]]; then
	psql_x="[\"s1.area JOIN: left join store s1 on a.store_id = s1.store_id\",\"b7.categoryname_1nd JOIN: left join product p9 on a.product_id = p9.product_id left join 1nd_category b7 on p9.categoryid_1nd = b7.categoryid_1nd\",\"s2.store_name JOIN: left join store s2 on a.store_id = s2.store_id\",\"b8.categoryname_2nd JOIN: left join product p10 on a.product_id = p10.product_id left join 2nd_category b8 on p10.categoryid_1nd = b8.categoryid_1nd and p10.categoryid_2nd = b8.categoryid_2nd\"]"
fi

results=`$MYSQL -s -N -e "select group_concat(filter) from ${OUTPUT_TABLE}"`
IFS=',' read -a results <<< ${results};
psql_app="WorkingHouse"
psql_event="WorkingHouse_drilldown"
psql_table="trans_record"


psql_timestamp="\"trans_date\""

psql_exclusive="\"\""
psql_y1="\"\""
psql_calculation1="\"\""
values_a="_"
values_b="_"

for (( i=0 ; i<${#results[@]} ; i++ ))
do
	psql_id_a="${results[$i]}a-${POSTFIX}"
	psql_token_a=$psql_id_a
	psql_startDate="\"${DATE_FROM}\""
	psql_endDate="\"${DATE_END}\""
	psql_y_a="\"qty\""
	psql_calculation_a="\"sum\""
	psql_filter="\"${results[$i]//=/;}\""
	
	psql_time="\"trans_date\""
	value_a="('${psql_id_a}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_a},\"calculation\":${psql_calculation_a},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter},\"track\":\"\",\"time\":${psql_time},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_a}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true,'','x,y,date,time,grouping,groupingList,axis,keyword','bar,hbar,stackedbar,percentagebar,line,heatmap,treemap,gmap,pie,radar,sankey,pareto,table,wordcloud','','','csv_result')"
	values_a="${values_a},${value_a}"
	
	psql_id_b="${results[$i]}b-${POSTFIX}"
	psql_token_b=$psql_id_b
	psql_y_b="\"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt\""
	psql_calculation_b="\"trans_price\""
	value_b="('${psql_id_b}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_b},\"calculation\":${psql_calculation_b},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter},\"track\":\"\",\"time\":${psql_time},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_b}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true,'','x,y,date,time,grouping,groupingList,axis,keyword','bar,hbar,stackedbar,percentagebar,line,heatmap,treemap,gmap,pie,radar,sankey,pareto,table,wordcloud','','','csv_result')"
	values_b="${values_b},${value_b}"
	
done
#echo "insert into bo_data_link values ${values_a//_,/}"
PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_a//_,/}"
PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_b//_,/}"