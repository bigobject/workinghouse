#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

#DATE="2020-01-01~2020-05-31"
#POSTFIX='test'
#FILTER="台北市"
#FILTER1=""
#SHOWOPTION="bbb"

#MYSQL="mysql -h 127.0.0.1 -P 3306"
#tmp_table="SaleStore_table_tmp_$POSTFIX"
OUTPUT_TABLE="SearchMem_table_$POSTFIX"

DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}

#prevent sql injection


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
FILTER="${FILTER//,/\',\'}"

if [[ $FILTER != '' ]]; then
	echo "create table $OUTPUT_TABLE as (cluster  select concat(substr(VIP_cardnum, 0, 6),'****')as VIP_cardnum,count(*)as qty,sum(trans_price) as renvenue  from trans_head A left join trans_record B on A.trans_id = B.trans_id inner join (select product_id ,b.product_name as product_name,b.feature as feature,b.brief as brief from (select trans_id,product_id from trans_record where trans_date>='${DATE_FROM}'and  trans_date<='${DATE_END}' group by product_id) a inner join(select *,rowid()as weight from (select product_id,product_name,feature,brief from product filter by match(feature,'$FILTER'))) b on a.product_id = b.product_id order by b.weight asc limit 10)C on B.product_id = C.product_id where A.trans_date>='${DATE_FROM}'and  A.trans_date<='${DATE_END}'and  A.VIP_cardnum <> ''group by VIP_cardnum order by qty desc ,renvenue desc limit 100)"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select concat(substr(VIP_cardnum, 0, 6),'****')as VIP_cardnum,count(*)as qty,sum(trans_price) as renvenue  from trans_head A left join trans_record B on A.trans_id = B.trans_id inner join (select product_id ,b.product_name as product_name,b.feature as feature,b.brief as brief from (select trans_id,product_id from trans_record where trans_date>='${DATE_FROM}'and  trans_date<='${DATE_END}' group by product_id) a inner join(select *,rowid()as weight from (select product_id,product_name,feature,brief from product filter by match(feature,'$FILTER'))) b on a.product_id = b.product_id order by b.weight asc limit 10)C on B.product_id = C.product_id where A.trans_date>='${DATE_FROM}'and  A.trans_date<='${DATE_END}'and  A.VIP_cardnum <> ''group by VIP_cardnum order by qty desc ,renvenue desc limit 100)"
else 
	echo "create table $OUTPUT_TABLE as (cluster  select concat(substr(VIP_cardnum, 0, 6),'****')as VIP_cardnum,count(*)as qty,sum(trans_price) as renvenue  from trans_head A left join trans_record B on A.trans_id = B.trans_id inner join ( select product_id,b.product_name as product_name,b.feature as feature,b.brief as brief from (select product_id,count(*)as qty  from trans_record where trans_date>='${DATE_FROM}' and  trans_date<='${DATE_END}'  group by product_id order by qty desc  limit 10) a left join product b on a.product_id = b.product_id)C on B.product_id = C.product_id where A.trans_date>='${DATE_FROM}'and  A.trans_date<='${DATE_END}'and  A.VIP_cardnum <> ''group by VIP_cardnum order by qty desc ,renvenue desc limit 100)"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select concat(substr(VIP_cardnum, 0, 6),'****')as VIP_cardnum,count(*)as qty,sum(trans_price) as renvenue  from trans_head A left join trans_record B on A.trans_id = B.trans_id inner join ( select product_id,b.product_name as product_name,b.feature as feature,b.brief as brief from (select product_id,count(*)as qty  from trans_record where trans_date>='${DATE_FROM}' and  trans_date<='${DATE_END}'  group by product_id order by qty desc  limit 10) a left join product b on a.product_id = b.product_id)C on B.product_id = C.product_id where A.trans_date>='${DATE_FROM}'and  A.trans_date<='${DATE_END}'and  A.VIP_cardnum <> ''group by VIP_cardnum order by qty desc ,renvenue desc limit 100)"
fi 