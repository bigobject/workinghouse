#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

#DATE="2020-01-01~2020-05-31"
#POSTFIX='test'
#FILTER="台北市"
#FILTER1=""
#SHOWOPTION="bbb"

#MYSQL="mysql -h 127.0.0.1 -P 3306"
#tmp_table="SaleStore_table_tmp_$POSTFIX"
echo "Auto_pilot"
compare_days='14d'
compare_int=14
OUTPUT_TABLE="Search_table_$POSTFIX"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~} 

DATE_BACK=`$MYSQL -s -N -e "select cast(subtime(str_to_datetime32('$DATE_END','%y-%m-%d'),'$compare_days') as date32);"`


#prevent sql injection


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
echo "create table $OUTPUT_TABLE as (select area,renvenue-B.avg_renvenue as change_amont,sqrt(((renvenue-B.avg_renvenue)*1.00/B.avg_renvenue)*((renvenue-B.avg_renvenue)*1.00/B.avg_renvenue)) as change_per from (select b.area as area,sum(trans_price) as renvenue from trans_record a left join store b on a.store_id = b.store_id where a.trans_date>='$DATE_END' group by b.area)A left join (select area, round(renvenue*1.00/$compare_int,0) as avg_renvenue from (select b.area as area,sum(trans_price) as renvenue, from trans_record a left join store b on a.store_id = b.store_id where a.trans_date>='$DATE_BACK' and a.trans_date<'$DATE_END' group by b.area))B on A.area = B.area);"
$MYSQL -e "create table $OUTPUT_TABLE as (select area,renvenue-B.avg_renvenue as change_amont,sqrt(((renvenue-B.avg_renvenue)*1.00/B.avg_renvenue)*((renvenue-B.avg_renvenue)*1.00/B.avg_renvenue)) as change_per from (select b.area as area,sum(trans_price) as renvenue from trans_record a left join store b on a.store_id = b.store_id where a.trans_date>='$DATE_END' group by b.area)A left join (select area, round(renvenue*1.00/$compare_int,0) as avg_renvenue from (select b.area as area,sum(trans_price) as renvenue, from trans_record a left join store b on a.store_id = b.store_id where a.trans_date>='$DATE_BACK' and a.trans_date<'$DATE_END' group by b.area))B on A.area = B.area);"

