#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin


###CHANGE if you have special requirement
ShareVariables=0 #set 1 to get output variables from TimeLiveBI
ResultExclude="	0" #the result will filter out whatever you set (string)
UseCache=0 #set 1 to create cache to increase loading speed
REMOTECLUSTER="cluster" #set cluster or remote select
PICK_TRACKER=0 #pick tracker to create track group
# OUTPUTTABLE="TimeLiveBI_LineOverall" #output table name
split_symbol="⋂" #split symbol for each level
BOIP=${BO%%:*}
BOPORT=${BO#*:}



start=$(date +'%s')


IFS='|' read -a X_ARY <<< ${X_PARAMS//	/|};

/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "$OUTPUTTABLE" -PARTITION_DB "$PARTITION_DB" -PARTITION_PUSHDOWNDB "$PARTITION_PUSHDOWNDB" -PARTITION_TIME "$PARTITION_TIME" -TABLE "$TABLE_PARAM" -X "${X_ARY[${#X_ARY[@]}-1]}" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "" -TRACK "" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"

mysql -h $BOIP -e "update ${OUTPUTTABLE}_${POSTFIX} set filter = 'OVERALL'"


echo "It took $(($(date +'%s') - $start)) seconds"
