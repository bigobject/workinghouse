#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

OUTPUT_TABLE="weekday_hour_${POSTFIX}"


#DATE_FROM=${DATE%%~*}
#DATE_END=${DATE#*~}

DATE_value=`$MYSQL -s -N -e "select name from date_list where value='${DATE}'"`
echo "---------"$OUTPUT_TABLE
echo "${DATE}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1  from (select store_id,sum(qty) as qty,sum(trans_price) as renvenue  from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1  from (select store_id,sum(qty) as qty,sum(trans_price) as renvenue  from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select trans_weekday,sum(qty) as qty,trans_hour from (select store_id,product_id,qty,hour(trans_datetime) as trans_hour,concat(weekday(trans_date),'-',date_format(trans_date,'%A')) as trans_weekday,date_format(trans_date,'%Y%m') as year_month from trans_record where year_month='${DATE_value}') a left join (select product_id,categoryid_1nd,categoryid_2nd,categoryid_3nd from product) b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd group by trans_weekday,trans_hour order by trans_weekday,trans_hour)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select trans_weekday,sum(qty) as qty,trans_hour from (select store_id,product_id,qty,hour(trans_datetime) as trans_hour,concat(weekday(trans_date),'-',date_format(trans_date,'%A')) as trans_weekday,date_format(trans_date,'%Y%m') as year_month from trans_record where year_month='${DATE_value}') a left join (select product_id,categoryid_1nd,categoryid_2nd,categoryid_3nd from product) b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd group by trans_weekday,trans_hour order by trans_weekday,trans_hour)"
	fi
		
fi
