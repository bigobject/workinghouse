#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

#DATE="2019-12-01~2019-12-31"
#POSTFIX='test'
#SHOWOPTION="bbb"

#MYSQL="mysql -h 127.0.0.1 -P 3306"
echo "$MYSQL"
tmp_table="SaleStore_table_tmp_$POSTFIX"
OUTPUT_TABLE="SaleStore_table_$POSTFIX"

DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}
SHOWOPTION=${SHOWOPTION}

#prevent sql injection


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select b.store_name as store_name,concat('<a href=\"../../api/v1/link/',store_name,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_name,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,rowid(),b.area as area from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select b.store_name as store_name,concat('<a href=\"../../api/v1/link/',store_name,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_name,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,rowid(),b.area as area from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from (select store_id,product_id,qty,trans_price, bb.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)"
		##要注意密碼
		#PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link_log where link_id in (select link_id from bo_data_link_log a inner join bo_data_link b on a.link_id = b.id where b.ticket = '$POSTFIX')"
		#PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link where ticket = '$POSTFIX'"
		#
		#results=`$MYSQL -s -N -e "select group_concat(s) from (select store_area as s from ${OUTPUT_TABLE} order by rowid()asc)"`
		#IFS=',' read -a results <<< ${results};
		#psql_app="WorkingHouse"
		#psql_event="WorkingHouse_drilldown"
		#psql_table="trans_record a left join (select *,cast(substr(year(trans_date), 3, 2) as string(12)) as year1 from trans_head) m on a.trans_id = m.trans_id left join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id TABLES:trans_record"
		#psql_x="[\"s.area\",\"s.store_name\"]"
		#
		#psql_timestamp="\"trans_date\""
		##psql_x="[\"flag\",''DATE_FORMAT(trans_time,\"%Y/%m/%d\") GROUPKEY: year(trans_time),month(trans_time),day(trans_time)'']"
		#
		##單引號前面再加上一個單引號，即可跳脫
		#psql_exclusive="\"\""
		#psql_y1="\"\""
		#psql_calculation1="\"\""
		#values_a="_"
		#values_b="_"
		#
		#for (( i=0 ; i<${#results[@]} ; i++ ))
		#do
		#	psql_id_a="${results[$i]}a-${POSTFIX}"
		#	psql_token_a=$psql_id_a
		#	psql_startDate="\"${DATE_FROM}\""
		#	psql_endDate="\"${DATE_END}\""
		#	psql_y_a="\"qty\""
		#	psql_calculation_a="\""sum"\""
		#	psql_filter_a="\"${results[$i]}\""
		#	
		#	psql_time_a="\"trans_date\""
		#	value_a="('${psql_id_a}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_a},\"calculation\":${psql_calculation_a},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_a},\"track\":\"\",\"time\":${psql_time_a},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_a}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
		#	values_a="${values_a},${value_a}"
		#	
		#	psql_id_b="${results[$i]}b-${POSTFIX}"
		#	psql_token_b=$psql_id_b
		#	psql_y_b="\"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt\""
		#	psql_calculation_b="\""trans_price"\""
		#	psql_filter_b="\"${results[$i]}\""
		#	psql_time_b="\""trans_date"\""
		#	value_b="('${psql_id_b}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_b},\"calculation\":${psql_calculation_b},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_b},\"track\":\"\",\"time\":${psql_time_b},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_b}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
		#	values_b="${values_b},${value_b}"
		#	
		#done
		#echo "PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c \"insert into bo_data_link values ${values_a//_,/}\""
		#PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_a//_,/}"
		#PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_b//_,/}"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select store_name,concat('<a href=\"../../api/v1/link/',store_name,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_name,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,rowid(),area from ( select store_name,format_num(qty,0) as  qty,format_num(renvenue,0) as renvenue,rowid(),area from(select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 ,b.area as area  from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)))"
		
		#$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area))"

		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select store_name,concat('<a href=\"../../api/v1/link/',store_name,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_name,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,rowid(),area from ( select store_name,format_num(qty,0) as  qty,format_num(renvenue,0) as renvenue,rowid(),area from(select b.store_name as store_name,qty,renvenue,'aa' as filter ,'bb' as filter1 ,b.area as area  from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')order by b.area)))"
		
		
		
		#要注意密碼
		PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link_log where link_id in (select link_id from bo_data_link_log a inner join bo_data_link b on a.link_id = b.id where b.ticket = '$POSTFIX')"
		PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link where ticket = '$POSTFIX'"
		
		results=`$MYSQL -s -N -e " select group_concat(s) from (select concat(area,';',store_name) as s from ${OUTPUT_TABLE} order by rowid()asc)"`
		IFS=',' read -a results <<< ${results};
		psql_app="WorkingHouse"
		psql_event="WorkingHouse_drilldown"
		psql_table="trans_record a left join (select *,cast(substr(year(trans_date), 3, 2) as string(12)) as year1 from trans_head) m on a.trans_id = m.trans_id left join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id TABLES:trans_record"
		psql_x="[\"s.area\",\"s.store_name\",\"b7.categoryname_1nd JOIN: left join 1nd_category b7 on a.p.categoryid_1nd = b7.categoryid_1nd\"]"
		echo 'V'
		psql_timestamp="\"trans_date\""
		#psql_x="[\"flag\",''DATE_FORMAT(trans_time,\"%Y/%m/%d\") GROUPKEY: year(trans_time),month(trans_time),day(trans_time)'']"
		
		#單引號前面再加上一個單引號，即可跳脫
		psql_exclusive="\"\""
		psql_y1="\"\""
		psql_calculation1="\"\""
		values_a="_"
		values_b="_"
		
		for (( i=0 ; i<${#results[@]} ; i++ ))
		do
			psql_id_a="${results[$i]}a-${POSTFIX}"
			psql_token_a=$psql_id_a
			psql_startDate="\"${DATE_FROM}\""
			psql_endDate="\"${DATE_END}\""
			psql_y_a="\"qty\""
			psql_calculation_a="\"sum\""
			psql_filter_a="\"${results[$i]}\""
			
			psql_time_a="\"trans_date\""
			value_a="('${psql_id_a}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_a},\"calculation\":${psql_calculation_a},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_a},\"track\":\"\",\"time\":${psql_time_a},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_a}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
			values_a="${values_a},${value_a}"
			
			psql_id_b="${results[$i]}b-${POSTFIX}"
			psql_token_b=$psql_id_b
			psql_y_b="\"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt\""
			psql_calculation_b="\"trans_price\""
			psql_filter_b="\"${results[$i]}\""
			psql_time_b="\"trans_date\""
			value_b="('${psql_id_b}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_b},\"calculation\":${psql_calculation_b},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_b},\"track\":\"\",\"time\":${psql_time_b},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_b}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
			values_b="${values_b},${value_b}"
			
		done
		echo "PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c \"insert into bo_data_link values ${values_a//_,/}\""
		PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_a//_,/}"
		PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_b//_,/}"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo  "create table $OUTPUT_TABLE as (cluster select  store_area,concat('<a href=\"../../api/v1/link/',store_area,'a-${POSTFIX}?format=html\" target=\"123\">',format_num(qty,0),'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_area,'b-${POSTFIX}?format=html\" target=\"123\">',format_num(renvenue,0),'</a>') as renvenue,rowid(),categoryname_1nd from(select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,categoryname_1nd from (select store_id,sum(qty) as qty,sum(trans_price) as renvenue ,categoryname_1nd from (select store_id,product_id,qty,trans_price,cc.categoryname_1nd as categoryname_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id left join 1nd_category cc on bb.categoryid_1nd = cc.categoryid_1nd where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select  store_area,concat('<a href=\"../../api/v1/link/',store_area,'a-${POSTFIX}?format=html\" target=\"123\">',format_num(qty,0),'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_area,'b-${POSTFIX}?format=html\" target=\"123\">',format_num(renvenue,0),'</a>') as renvenue,rowid(),categoryname_1nd from(select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,categoryname_1nd from (select store_id,sum(qty) as qty,sum(trans_price) as renvenue ,categoryname_1nd from (select store_id,product_id,qty,trans_price,cc.categoryname_1nd as categoryname_1nd from (select store_id,product_id,qty,trans_price from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}') aa left join (select product_id,categoryid_1nd from product) bb on aa.product_id = bb.product_id left join 1nd_category cc on bb.categoryid_1nd = cc.categoryid_1nd where bb.categoryid_1nd in ('$FILTER1')) group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
		#要注意密碼
		PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link_log where link_id in (select link_id from bo_data_link_log a inner join bo_data_link b on a.link_id = b.id where b.ticket = '$POSTFIX')"
		PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link where ticket = '$POSTFIX'"
		
		results=`$MYSQL -s -N -e "select group_concat(s) from (select concat(store_area,';',categoryname_1nd) as s from ${OUTPUT_TABLE} order by rowid()asc)"`
		IFS=',' read -a results <<< ${results};
		psql_app="WorkingHouse"
		psql_event="WorkingHouse_drilldown"
		psql_table="trans_record a left join (select *,cast(substr(year(trans_date), 3, 2) as string(12)) as year1 from trans_head) m on a.trans_id = m.trans_id left join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id TABLES:trans_record"
		psql_x="[\"s.area\",\"b7.categoryname_1nd JOIN: left join 1nd_category b7 on a.p.categoryid_1nd = b7.categoryid_1nd\",\"s.store_name\"]"
		
		psql_timestamp="\"trans_date\""
		#psql_x="[\"flag\",''DATE_FORMAT(trans_time,\"%Y/%m/%d\") GROUPKEY: year(trans_time),month(trans_time),day(trans_time)'']"
		
		#單引號前面再加上一個單引號，即可跳脫
		psql_exclusive="\"\""
		psql_y1="\"\""
		psql_calculation1="\"\""
		values_a="_"
		values_b="_"
		
		for (( i=0 ; i<${#results[@]} ; i++ ))
		do
			psql_id_a="${results[$i]}a-${POSTFIX}"
			psql_token_a=$psql_id_a
			psql_startDate="\"${DATE_FROM}\""
			psql_endDate="\"${DATE_END}\""
			psql_y_a="\"qty\""
			psql_calculation_a="\"sum\""
			psql_filter_a="\"${results[$i]}\""
			
			psql_time_a="\"trans_date\""
			value_a="('${psql_id_a}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_a},\"calculation\":${psql_calculation_a},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_a},\"track\":\"\",\"time\":${psql_time_a},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_a}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
			values_a="${values_a},${value_a}"
			
			psql_id_b="${results[$i]}b-${POSTFIX}"
			psql_token_b=$psql_id_b
			psql_y_b="\"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt\""
			psql_calculation_b="\""trans_price"\""
			psql_filter_b="\"${results[$i]}\""
			psql_time_b="\"trans_date\""
			value_b="('${psql_id_b}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_b},\"calculation\":${psql_calculation_b},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_b},\"track\":\"\",\"time\":${psql_time_b},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_b}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
			values_b="${values_b},${value_b}"
			
		done
		echo "PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c \"insert into bo_data_link values ${values_a//_,/}\""
		PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_a//_,/}"
		PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_b//_,/}"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select store_area,concat('<a href=\"../../api/v1/link/',store_area,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_area,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,rowid() from (select store_area,format_num(qty,0) as  qty,format_num(renvenue,0) as renvenue from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc)))"
		
		#$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,rowid() from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc))"
		
		
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select store_area,concat('<a href=\"../../api/v1/link/',store_area,'a-${POSTFIX}?format=html\" target=\"123\">',qty,'</a>') as qty,concat('<a href=\"../../api/v1/link/',store_area,'b-${POSTFIX}?format=html\" target=\"123\">',renvenue,'</a>') as renvenue,rowid() from (select store_area,format_num(qty,0) as  qty,format_num(renvenue,0) as renvenue from (select b.area as store_area,sum(qty) as qty,sum(renvenue) as renvenue,'aa' as filter ,'bb' as filter1 from (select store_id,sum(qty) as qty ,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and trans_date<='${DATE_END}' group by store_id) a left join store b on a.store_id = b.store_id group by b.area order by b.area asc)))"
		
		#要注意密碼
		PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link_log where link_id in (select link_id from bo_data_link_log a inner join bo_data_link b on a.link_id = b.id where b.ticket = '$POSTFIX')"
		PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link where ticket = '$POSTFIX'"
		
		results=`$MYSQL -s -N -e "select group_concat(s) from (select store_area as s from ${OUTPUT_TABLE} order by rowid()asc)"`
		IFS=',' read -a results <<< ${results};
		psql_app="WorkingHouse"
		psql_event="WorkingHouse_drilldown"
		psql_table="trans_record a left join (select *,cast(substr(year(trans_date), 3, 2) as string(12)) as year1 from trans_head) m on a.trans_id = m.trans_id left join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id TABLES:trans_record"
		psql_x="[\"s.area\",\"s.store_name\"]"
		
		psql_timestamp="\"trans_date\""
		#psql_x="[\"flag\",''DATE_FORMAT(trans_time,\"%Y/%m/%d\") GROUPKEY: year(trans_time),month(trans_time),day(trans_time)'']"
		
		#單引號前面再加上一個單引號，即可跳脫
		psql_exclusive="\"\""
		psql_y1="\"\""
		psql_calculation1="\"\""
		values_a="_"
		values_b="_"
		
		for (( i=0 ; i<${#results[@]} ; i++ ))
		do
			psql_id_a="${results[$i]}a-${POSTFIX}"
			psql_token_a=$psql_id_a
			psql_startDate="\"${DATE_FROM}\""
			psql_endDate="\"${DATE_END}\""
			psql_y_a="\"qty\""
			psql_calculation_a="\"sum\""
			psql_filter_a="\"${results[$i]}\""
			
			psql_time_a="\"trans_date\""
			value_a="('${psql_id_a}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_a},\"calculation\":${psql_calculation_a},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_a},\"track\":\"\",\"time\":${psql_time_a},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawDatMoa\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_a}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
			values_a="${values_a},${value_a}"
			
			psql_id_b="${results[$i]}b-${POSTFIX}"
			psql_token_b=$psql_id_b
			psql_y_b="\"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt\""
			psql_calculation_b="\"trans_price\""
			psql_filter_b="\"${results[$i]}\""
			psql_time_b="\"trans_date\""
			value_b="('${psql_id_b}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_b},\"calculation\":${psql_calculation_b},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter_b},\"track\":\"\",\"time\":${psql_time_b},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_b}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true)"
			values_b="${values_b},${value_b}"
			
		done
		echo "PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c \"insert into bo_data_link values ${values_a//_,/}\""
		PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_a//_,/}"
		PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_b//_,/}"
		
	fi
		
fi 