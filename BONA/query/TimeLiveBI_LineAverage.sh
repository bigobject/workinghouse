#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin


###CHANGE if you have special requirement
ShareVariables=0 #set 1 to get output variables from TimeLiveBI
ResultExclude="	0" #the result will filter out whatever you set (string)
UseCache=0 #set 1 to create cache to increase loading speed
REMOTECLUSTER="cluster" #set cluster or remote select
PICK_TRACKER=0 #pick tracker to create track group
# OUTPUTTABLE="TimeLiveBI_LineAverage" #output table name
split_symbol="⋂" #split symbol for each level
BOIP=${BO%%:*}
BOPORT=${BO#*:}



start=$(date +'%s')


/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "TimeLiveBI_Linetemp" -PARTITION_DB "$PARTITION_DB" -PARTITION_PUSHDOWNDB "$PARTITION_PUSHDOWNDB" -PARTITION_TIME "$PARTITION_TIME" -TABLE "$TABLE_PARAM" -X "$X_PARAMS" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "$FILTER_PARAMS" -TRACK "$TRACK_TICKET" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"

mysql -h $BOIP -e "update TimeLiveBI_Linetemp_${POSTFIX} a inner join (select filter,avg(y) as y from TimeLiveBI_Linetemp_${POSTFIX} group by filter) b on a.filter = b.filter set y = b.y"
mysql -h $BOIP -e "drop table ${OUTPUTTABLE}_${POSTFIX}" || true
mysql -h $BOIP -e "create table ${OUTPUTTABLE}_${POSTFIX} as (select x,y,concat(filter,'_avg') as filter, subgroup from TimeLiveBI_Linetemp_${POSTFIX})"

echo "It took $(($(date +'%s') - $start)) seconds"
