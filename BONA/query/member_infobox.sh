#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

OUTPUT_TABLE="member_infobox_${POSTFIX}"


#DATE_FROM=${DATE%%~*}
#DATE_END=${DATE#*~}

DATE_value=`$MYSQL -s -N -e "select name from date_list where value='${DATE}'"`

Last_month=`$MYSQL -s -N -e "cluster select cast(year_month as int )-1 from trans_record where year_month = '${DATE_value}' group by year_month"`

echo "---------"$OUTPUT_TABLE
echo "${DATE}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "create table $OUTPUT_TABLE as (cluster select '新增會員成長率' as TITLE,concat(round((qty-b.qty)*1.00/b.qty,4)*100,'%') as VALUE,'A' as bakcground from (select 'New_member' as New_member,count(*) as qty  from (select Create_store,date_format(Createdate,'%Y%m') as year_month from VIP_member a where year_month in ('${DATE_value}'))aa left join store bb on aa.Create_store = bb.store_id where bb.area in('$FILTER'))a left join (select 'New_member' as New_member,count(*) as qty  from (select Create_store,date_format(Createdate,'%Y%m') as year_month from VIP_member a where year_month in ('${Last_month}'))aa left join store bb on aa.Create_store = bb.store_id where bb.area in('$FILTER'))b on a.New_member = b.New_member)"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster select '新增會員成長率' as TITLE,concat(round((qty-b.qty)*1.00/b.qty,4)*100,'%') as VALUE,'A' as bakcground from (select 'New_member' as New_member,count(*) as qty  from (select Create_store,date_format(Createdate,'%Y%m') as year_month from VIP_member a where year_month in ('${DATE_value}'))aa left join store bb on aa.Create_store = bb.store_id where bb.area in('$FILTER'))a left join (select 'New_member' as New_member,count(*) as qty  from (select Create_store,date_format(Createdate,'%Y%m') as year_month from VIP_member a where year_month in ('${Last_month}'))aa left join store bb on aa.Create_store = bb.store_id where bb.area in('$FILTER'))b on a.New_member = b.New_member)"
else 
	
	echo "create table $OUTPUT_TABLE as (cluster select '新增會員成長率' as TITLE,concat(round((qty-b.qty)*1.00/b.qty,4)*100,'%') as VALUE,'A' as bakcground from (select 'New_member' as New_member,count(*) as qty  from (select date_format(Createdate,'%Y%m') as year_month from VIP_member where  year_month in ('${DATE_value}')))a left join (select 'New_member' as New_member,count(*) as qty  from (select date_format(Createdate,'%Y%m') as year_month from VIP_member where  year_month in ('${Last_month}')))b on a.New_member = b.New_member)"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster select '新增會員成長率' as TITLE,concat(round((qty-b.qty)*1.00/b.qty,4)*100,'%') as VALUE,'A' as bakcground from (select 'New_member' as New_member,count(*) as qty  from (select date_format(Createdate,'%Y%m') as year_month from VIP_member where  year_month in ('${DATE_value}')))a left join (select 'New_member' as New_member,count(*) as qty  from (select date_format(Createdate,'%Y%m') as year_month from VIP_member where  year_month in ('${Last_month}')))b on a.New_member = b.New_member)"
fi


