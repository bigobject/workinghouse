#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"
OUTPUT_TABLE="area_list2"
DATE_value=`$MYSQL -s -N -e "select name from date_list where value='${DATE}'"`

#Cluster_Partition=" partition by alias='internal-prod-bo02.storm.mg' and type='master'"
#group by name
echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
echo "create table $OUTPUT_TABLE as (cluster select b.area as header,b.area as value  from (select store_id from trans_record where year_month = '${DATE_value}') a left join (select store_id,area from store) b on a.store_id = b.store_id group by header order by header asc);"
$MYSQL -e "create table $OUTPUT_TABLE as (cluster select b.area as header,b.area as value  from (select store_id from trans_record where year_month = '${DATE_value}') a left join (select store_id,area from store) b on a.store_id = b.store_id group by header order by header asc);"

