#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

OUTPUT_TABLE="diff_infobox_${POSTFIX}"


#DATE_FROM=${DATE%%~*}
#DATE_END=${DATE#*~}

DATE_value=`$MYSQL -s -N -e "select name from date_list where value='${DATE}'"`

Last_month=`$MYSQL -s -N -e "cluster select cast(year_month as int )-1 from trans_record where year_month = '${DATE_value}' group by year_month"`



echo "---------"$OUTPUT_TABLE
echo "${DATE}"
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true

if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')and c.categoryid_1nd in ('$FILTER1')) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')and c.categoryid_1nd in ('$FILTER1')) b on a.TITLE = b.TITLE)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')and c.categoryid_1nd in ('$FILTER1')) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')and c.categoryid_1nd in ('$FILTER1')) b on a.TITLE = b.TITLE)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')) b on a.TITLE = b.TITLE)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where b.area in ('$FILTER')) b on a.TITLE = b.TITLE)"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where c.categoryid_1nd in ('$FILTER1') ) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where c.categoryid_1nd in ('$FILTER1')) b on a.TITLE = b.TITLE)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where c.categoryid_1nd in ('$FILTER1') ) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id where c.categoryid_1nd in ('$FILTER1')) b on a.TITLE = b.TITLE)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id ) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id) b on a.index = b.index)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select '上月營收差異' as TITLE,VALUE-b.VALUE as VALUE,case when VALUE>0 then 'A' else 'B' end as background from (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${DATE_value}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id ) a left join (select '本月營收' as TITLE ,sum(trans_price) as VALUE,rowid() as index from (select store_id,product_id,trans_price from trans_record where year_month in ('${Last_month}'))a left join store b on a.store_id = b.store_id left join product c on a.product_id = c.product_id) b on a.index = b.index)"
	fi
		
fi
