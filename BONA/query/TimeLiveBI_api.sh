#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin

BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP" #change ip and port

#decode
API_INPUT=$(printf '%b' "${API_INPUT//%/\\x}")
echo "$API_INPUT"
#prevent sql injection
IFS=';' read -a API_INPUT <<< $API_INPUT;

if [[ ! -z $API_INPUT ]]; then
	APIINPUTCONDITION="where $API_INPUT"
fi
RESULTTABLE=$OUTPUTTABLE
export OUTPUTTABLE="TimeLiveBI"
/webapp/query/TimeLiveBI.sh

$MYSQL -e " drop table ${RESULTTABLE}_${POSTFIX}" || true
echo "create table ${RESULTTABLE}_${POSTFIX} as (select * from TimeLiveBI_$POSTFIX $APIINPUTCONDITION)"
$MYSQL -e "create table ${RESULTTABLE}_${POSTFIX} as (select * from TimeLiveBI_$POSTFIX $APIINPUTCONDITION)"
	
echo "END"

