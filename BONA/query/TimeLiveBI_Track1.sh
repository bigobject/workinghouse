#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin


###CHANGE if you have special requirement
ShareVariables=0 #set 1 to get output variables from TimeLiveBI
ResultExclude="	0" #the result will filter out whatever you set (string)
UseCache=0 #set 1 to create cache to increase loading speed
REMOTECLUSTER="cluster" #set cluster or remote select
PICK_TRACKER=1 #1: pick tracker to create track group  2: set up advanced track
OUTPUTTABLE="Track" #output table name
split_symbol="⋂" #split symbol for each level
BOIP=${BO%%:*}
BOPORT=${BO#*:}

start=$(date +'%s')
if [[ ${X_PARAMS//"a.age"/} != ${X_PARAMS} ]];then
	if [[ ${Y_PARAMS//"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt"/} != ${Y_PARAMS} ]];then
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"(select *,year(a.trans_date)-year(b3.birthday) as age from ${TABLE_PARAM%	*} a left join trans_head m on a.trans_id = m.trans_id left join VIP_member b3 on m.VIP_cardnum = b3.ID inner join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id) TABLES:${TABLE_PARAM%	*}"}
	else
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"(select *,year(a.trans_date)-year(b3.birthday) as age from ${TABLE_PARAM%	*} a left join trans_head m on a.trans_id = m.trans_id left join VIP_member b3 on m.VIP_cardnum = b3.ID) TABLES:${TABLE_PARAM%	*}"}
	fi
else
	if [[ ${Y_PARAMS//"m.VIP_cardnum"/} != ${Y_PARAMS} ]] || [[ -n ${TRACK_TICKET} ]];then
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"${TABLE_PARAM%	*} a left join trans_head m on a.trans_id = m.trans_id TABLES:${TABLE_PARAM%	*}"}
	fi
	
	if [[ ${Y_PARAMS//"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt"/} != ${Y_PARAMS} ]];then
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"${TABLE_PARAM%	*} a inner join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id TABLES:${TABLE_PARAM%	*}"}
	fi 

fi
if [[ ${X_PARAMS//"ELS: "/} != ${X_PARAMS} ]] && [[ -n $CONDITIONS ]];then
	TABLE_PARAM=${TABLE_PARAM//" product "/" (select * from product filter by match(feature,'${CONDITIONS}')) "}
	CONDITIONS=""
fi

if [[ $CONDITIONS == "intersection" ]] || [[ $CONDITIONS == "union" ]]; then
	PICK_TRACKER=2
	/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "$OUTPUTTABLE" -TABLE "$TABLE_PARAM" -X "$X_PARAMS" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "$FILTER_PARAMS" -TRACK "$TRACK_TICKET" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"
else
	# echo "$TABLE_PARAM" -ip bigobject
	/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "$OUTPUTTABLE" -TABLE "$TABLE_PARAM" -X "$X_PARAMS" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "$FILTER_PARAMS" -TRACK "$TRACK_TICKET" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"
fi


echo "It took $(($(date +'%s') - $start)) seconds"
