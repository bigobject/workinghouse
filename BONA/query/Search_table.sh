#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin

BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

#DATE="2020-01-01~2020-05-31"
#POSTFIX='test'
#FILTER="台北市"
#FILTER1=""
#SHOWOPTION="bbb"

#MYSQL="mysql -h 127.0.0.1 -P 3306"
#tmp_table="SaleStore_table_tmp_$POSTFIX"
OUTPUT_TABLE="Search_table_$POSTFIX"

DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}

#prevent sql injection


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
FILTER="${FILTER//,/\',\'}"

if [[ $FILTER != '' ]]; then
	echo "search"
	echo "create table $OUTPUT_TABLE as (cluster select concat('<b>',product_id,'</b>') as product_id ,concat('<b><a href=\"../../api/v1/link/',product_id,'a-${POSTFIX}?format=html\" target=\"123\">',product_name,'</b>') as product_name,concat('<b>',feature,'</b>') as feature,concat('<b>',qty,'</b>') as qty,concat('<b>',renvenue,'</b>') as renvenue,product_name as product_name1,product_id as product_id1 from ( select b.product_id as product_id ,b.product_name as product_name,b.feature as feature,b.brief as brief,format_num(qty,0) as qty ,format_num(renvenue,0) as renvenue from (select product_id,sum(qty) as qty,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}'and  trans_date<='${DATE_END}' group by product_id) a inner join(select *,rowid()as weight from (select product_id,product_name,feature,brief from product filter by match(feature,'$FILTER'))) b on a.product_id = b.product_id order by b.weight asc limit 100))"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster select concat('<b>',product_id,'</b>') as product_id ,concat('<b><a href=\"../../api/v1/link/',product_id,'a-${POSTFIX}?format=html\" target=\"123\">',product_name,'</b>') as product_name,concat('<b>',feature,'</b>') as feature,concat('<b>',qty,'</b>') as qty,concat('<b>',renvenue,'</b>') as renvenue,product_name as product_name1,product_id as product_id1 from ( select b.product_id as product_id ,b.product_name as product_name,b.feature as feature,b.brief as brief,format_num(qty,0) as qty ,format_num(renvenue,0) as renvenue from (select product_id,sum(qty) as qty,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}'and  trans_date<='${DATE_END}' group by product_id) a inner join(select *,rowid()as weight from (select product_id,product_name,feature,brief from product filter by match(feature,'$FILTER'))) b on a.product_id = b.product_id order by b.weight asc limit 100))"
else 
	echo "create table $OUTPUT_TABLE as (cluster  select concat('<b>',product_id,'</b>') as product_id ,concat('<b><a href=\"../../api/v1/link/',product_id,'a-${POSTFIX}?format=html\" target=\"123\">',product_name,'</b>') as product_name,concat('<b>',feature,'</b>') as feature,concat('<b>',qty,'</b>') as qty,concat('<b>',renvenue,'</b>') as renvenue,product_name as product_name1,product_id as product_id1 from (select product_id,b.product_name as product_name,b.feature as feature,b.brief as brief,format_num(qty,0) as qty ,format_num(renvenue,0) as renvenue from (select product_id,sum(qty) as qty,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and  trans_date<='${DATE_END}' group by product_id order by renvenue desc  limit 100) a left join product b on a.product_id = b.product_id ))"
	$MYSQL -e "create table $OUTPUT_TABLE as (cluster  select concat('<b>',product_id,'</b>') as product_id ,concat('<b><a href=\"../../api/v1/link/',product_id,'a-${POSTFIX}?format=html\" target=\"123\">',product_name,'</b>') as product_name,concat('<b>',feature,'</b>') as feature,concat('<b>',qty,'</b>') as qty,concat('<b>',renvenue,'</b>') as renvenue,product_name as product_name1,product_id as product_id1 from (select product_id,b.product_name as product_name,b.feature as feature,b.brief as brief,format_num(qty,0) as qty ,format_num(renvenue,0) as renvenue from (select product_id,sum(qty) as qty,sum(trans_price) as renvenue from trans_record where trans_date>='${DATE_FROM}' and  trans_date<='${DATE_END}' group by product_id order by renvenue desc  limit 100) a left join product b on a.product_id = b.product_id ))"
fi 



PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link_log where link_id in (select link_id from bo_data_link_log a inner join bo_data_link b on a.link_id = b.id where b.ticket = '$POSTFIX')"
PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_link where ticket = '$POSTFIX'"

results=`$MYSQL -s -N -e "select group_concat(product_name1) from ${OUTPUT_TABLE}"`
results1=`$MYSQL -s -N -e "select group_concat(product_id1) from ${OUTPUT_TABLE}"`



IFS=',' read -a results <<< ${results};
IFS=',' read -a results1 <<< ${results1};
psql_app="WorkingHouse"
psql_event="WorkingHouse_drilldown"
psql_table="trans_record"
psql_x="[\"p13.product_name JOIN: left join product p13 on a.product_id = p13.product_id\",\"s3.channel JOIN: left join store s3 on a.store_id = s3.store_id\"]"
psql_timestamp="\"trans_date\""

psql_exclusive="\"\""
psql_y1="\"\""
psql_calculation1="\"\""
values_a="_"


for (( i=0 ; i<${#results[@]} ; i++ ))
do
	
	psql_id_a="${results1[$i]}a-${POSTFIX}"
	psql_token_a=$psql_id_a
	psql_startDate="\"${DATE_FROM}\""
	psql_endDate="\"${DATE_END}\""
	psql_y_a="\"qty\""
	psql_calculation_a="\"sum\""
	
	psql_filter="\"${results[$i]//=/;}\""
	
	psql_time="\"trans_date\""
	value_a="('${psql_id_a}','${psql_app}','${POSTFIX}','{\"action\":\"drilldown\",\"event\":\"${psql_event}\",\"table\":\"${psql_table}\",\"timestamp\":${psql_timestamp},\"x\":${psql_x},\"y\":${psql_y_a},\"calculation\":${psql_calculation_a},\"exclusive\":${psql_exclusive},\"y1\":${psql_y1},\"calculation1\":${psql_calculation1},\"exclusive1\":null,\"group\":\"\",\"filter\":${psql_filter},\"track\":\"\",\"time\":${psql_time},\"startDate\":${psql_startDate},\"endDate\":${psql_endDate},\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"desc\",\"sort\":\"y\",\"dataType\":\"number\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','${psql_token_a}','WorkingHouse','',null,'2019-12-17 11:47:52.643','autogen','2019-12-17 11:47:52.643','autogen','AUTHENTICATED',true,true,true,'','x,y,date,time,grouping,groupingList,axis,keyword','bar,hbar,stackedbar,percentagebar,line,heatmap,treemap,gmap,pie,radar,sankey,pareto,table,wordcloud','','','csv_result')"
	
	values_a="${values_a},${value_a}"
	
done
#echo "insert into bo_data_link values ${values_a//_,/}"
#echo "${values_a//_,/}"
PGPASSWORD=DDKjgs8U psql -h ${DB} -U postgres -d bigobject -c "insert into bo_data_link values ${values_a//_,/};"

echo "done"

#if [[ $FILTER != '' ]]; then
#	echo "==========="
#	PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "delete from bo_data_track where id = '$POSTFIX'"
#	#id,app,name,desc,track_id,params,created_at,created_user_id,updated_at,updated_user_id,sub_tracks,is_public
#	track_value="('${POSTFIX}','WorkingHouse','目標產品搜尋:${FILTER}','','${track_id}','{\"action\":\"track\",\"event\":\"WorkingHouse_track\",\"table\":\"trans_record\",\"timestamp\":\"trans_date\",\"x\":[\"\"],\"y\":\"\",\"calculation\":\"\",\"exclusive\":\"\",\"y1\":"",\"calculation1\":\"\",\"exclusive1\":null,\"group\":\"\",\"filter\":\"\",\"track\":\"\",\"time\":\"\",\"startDate\":\"\",\"endDate\":\"\",\"baseDate\":null,\"toNow\":null,\"startTime\":\"00:00\",\"endTime\":\"23:59\",\"segment\":null,\"condition\":[],\"input\":null,\"analytic\":null,\"rawData\":false,\"chartId\":\"bar\",\"order\":\"\",\"sort\":\"\",\"dataType\":\"string\",\"max\":30,\"ymax\":null,\"ymin\":null,\"ystep\":null,\"xmax\":null,\"xmin\":null,\"fontStyle\":\"\",\"fontSize\":null,\"grouping\":{}}','2020-06-30 13:15:00','els','2020-06-30 13:15:00','els','[]',false)"
#	PGPASSWORD=DDKjgs8U psql -h ${DB}  -U postgres -d bigobject -c "insert into bo_data_track values $track_value"
#	track_table="Track_${POSTFIX}"
#	$MYSQL -e "drop table $track_table"|| true
#	$MYSQL -e "create table $track_table as (select product_id as TAKey,'' as subgroup,product_id as joinkey from $OUTPUT_TABLE)"
#fi