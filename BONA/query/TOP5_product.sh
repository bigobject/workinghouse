#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
BOIP=${BO%%:*}
BOPORT=${BO#*:}
MYSQL="mysql -h $BOIP"

OUTPUT_TABLE="TOP5_product_${POSTFIX}"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select B.product_name as product_name,sum(qty) as qty ,concat(B.categoryname_1nd,'-',area) as filter from(select store_id,product_id,qty,b.area as area, from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')) A inner join (select *,GROUP_ROW_NUMBER(categoryid_1nd) as rank from (select  product_id,b.product_name as product_name,sum(qty) as qty,d.categoryname_1nd as categoryname_1nd,d.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join (select categoryid_1nd,categoryname_1nd from 1nd_category) d on b.categoryid_1nd = d.categoryid_1nd where c.area in ('$FILTER') and b.categoryid_1nd in ('$FILTER1') group by b.categoryid_1nd,product_id order by b.categoryid_1nd,qty desc) where rank<=5)B on A.product_id = B.product_id group by product_id,area)"
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select B.product_name as product_name,sum(qty) as qty ,concat(B.categoryname_1nd,'-',area) as filter from(select store_id,product_id,qty,b.area as area, from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')) A inner join (select *,GROUP_ROW_NUMBER(categoryid_1nd) as rank from (select  product_id,b.product_name as product_name,sum(qty) as qty,d.categoryname_1nd as categoryname_1nd,d.categoryid_1nd as categoryid_1nd from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join (select categoryid_1nd,categoryname_1nd from 1nd_category) d on b.categoryid_1nd = d.categoryid_1nd where c.area in ('$FILTER') and b.categoryid_1nd in ('$FILTER1') group by b.categoryid_1nd,product_id order by b.categoryid_1nd,qty desc) where rank<=5)B on A.product_id = B.product_id group by product_id,area)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select B.product_name as product_name,sum(qty) as qty ,area as filter from (select store_id,product_id,qty,b.area as area, from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')) A inner join (select  product_id,b.product_name as product_name,sum(qty) as qty from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id where c.area in ('$FILTER')group by product_id order by qty desc limit 5)B on A.product_id = B.product_id group by product_id,area)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select B.product_name as product_name,sum(qty) as qty ,area as filter from (select store_id,product_id,qty,b.area as area, from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join store b on a.store_id = b.store_id where b.area in ('$FILTER')) A inner join (select  product_id,b.product_name as product_name,sum(qty) as qty from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id where c.area in ('$FILTER')group by product_id order by qty desc limit 5)B on A.product_id = B.product_id group by product_id,area)"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select product_name,qty,categoryname_1nd as filter,GROUP_ROW_NUMBER(categoryid_1nd) as rank, from (select product_id,sum(qty) as qty ,b.product_name as product_name,'全省' as filter,c.categoryname_1nd as categoryname_1nd,c.categoryid_1nd as categoryid_1nd  from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd where b.categoryid_1nd in ('$FILTER1') group by b.categoryid_1nd,product_id order by c.categoryid_1nd,qty desc ) where rank<=5)"
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select product_name,qty,categoryname_1nd as filter,GROUP_ROW_NUMBER(categoryid_1nd) as rank, from (select product_id,sum(qty) as qty ,b.product_name as product_name,'全省' as filter,c.categoryname_1nd as categoryname_1nd,c.categoryid_1nd as categoryid_1nd  from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd where b.categoryid_1nd in ('$FILTER1') group by b.categoryid_1nd,product_id order by c.categoryid_1nd,qty desc ) where rank<=5)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select product_name,qty, filter from (select product_id,sum(qty) as qty ,b.product_name as product_name,'全省' as filter from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id group by product_id order by qty desc )limit 5)"
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select product_name,qty, filter from (select product_id,sum(qty) as qty ,b.product_name as product_name,'全省' as filter from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id group by product_id order by qty desc )limit 5)"
	fi
		
fi
