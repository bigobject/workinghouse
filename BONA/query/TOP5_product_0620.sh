#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
MYSQL="mysql -h 192.168.1.73 -P 3306"

OUTPUT_TABLE="TOP5_product_${POSTFIX}"
DATE_FROM=${DATE%%~*}
DATE_END=${DATE#*~}


echo "---------"$OUTPUT_TABLE
$MYSQL -e "drop table $OUTPUT_TABLE;"|| true
if [[ $FILTER != '' ]]; then
	FILTER="${FILTER//,/\',\'}"
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select product_id,qty,product_name,filter1,GROUP_ROW_NUMBER(filter1) as rank from ( select product_id,sum(qty) as qty ,b.product_name as product_name,concat(c.area,'-',e.categoryname_1nd) as filter1,d.categoryid_1nd as categoryid_1nd ,e.categoryname_1nd as categoryname_1nd from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join product d on b.product_id = d.product_id left join 1nd_category e on d.categoryid_1nd = e.categoryid_1nd where c.area in ('$FILTER')  and categoryid_1nd in ('$FILTER1') group by product_id order by filter1,qty desc) where rank<=5)"
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select product_id,qty,product_name,filter1,GROUP_ROW_NUMBER(filter1) as rank from ( select product_id,sum(qty) as qty ,b.product_name as product_name,concat(c.area,'-',e.categoryname_1nd) as filter1,d.categoryid_1nd as categoryid_1nd ,e.categoryname_1nd as categoryname_1nd from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id left join product d on b.product_id = d.product_id left join 1nd_category e on d.categoryid_1nd = e.categoryid_1nd where c.area in ('$FILTER')  and categoryid_1nd in ('$FILTER1') group by product_id order by filter1,qty desc) where rank<=5)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select *,GROUP_ROW_NUMBER(filter1) as rank from ( select product_id,sum(qty) as qty ,b.product_name as product_name,c.area as filter1 from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id where c.area in ('$FILTER') group by product_id order by filter1,qty desc) where rank<=5)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,GROUP_ROW_NUMBER(filter1) as rank from ( select product_id,sum(qty) as qty ,b.product_name as product_name,c.area as filter1 from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join store c on a.store_id = c.store_id where c.area in ('$FILTER') group by product_id order by filter1,qty desc) where rank<=5)"
	fi
		
else
	echo "XD"
	echo "${FILTER}"
	if [[ $FILTER1 != '' ]]; then
		FILTER1="${FILTER1//,/\',\'}"
		echo "create table $OUTPUT_TABLE as (cluster select * ,GROUP_ROW_NUMBER(filter1) as rank from(select product_id,sum(qty) as qty ,b.product_name as product_name,c.categoryname_1nd as filter1 from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd where b.categoryid_1nd in ('$FILTER1') group by product_id order by filter1,qty desc ) where rank<=5)"
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select * ,GROUP_ROW_NUMBER(filter1) as rank from(select product_id,sum(qty) as qty ,b.product_name as product_name,c.categoryname_1nd as filter1 from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id left join 1nd_category c on b.categoryid_1nd = c.categoryid_1nd where b.categoryid_1nd in ('$FILTER1') group by product_id order by filter1,qty desc ) where rank<=5)"
	else 
		echo "create table $OUTPUT_TABLE as (cluster select *,GROUP_ROW_NUMBER(filter1) as rank from (select product_id,sum(qty) as qty ,b.product_name as product_name,'全省' as filter1 from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id group by product_id order by qty desc) where rank<=5)"
		
		$MYSQL -e "create table $OUTPUT_TABLE as (cluster select *,GROUP_ROW_NUMBER(filter1) as rank from (select product_id,sum(qty) as qty ,b.product_name as product_name,'全省' as filter1 from (select store_id,product_id,qty from trans_record where trans_date >='${DATE_FROM}' and trans_date<='${DATE_END}') a left join product b on a.product_id = b.product_id group by product_id order by qty desc) where rank<=5)"
	fi
		
fi
