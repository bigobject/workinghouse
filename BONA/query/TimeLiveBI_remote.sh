#!/bin/bash

set -e

PATH=$PATH:/usr/local/bin:/usr/bin:/bin


###CHANGE if you have special requirement
ShareVariables=0 #set 1 to get output variables from TimeLiveBI
ResultExclude="	0" #the result will filter out whatever you set (string)
UseCache=0 #set 1 to create cache to increase loading speed
REMOTECLUSTER="remote" #set cluster or remote select
PICK_TRACKER=0 #pick tracker to create track group
# OUTPUTTABLE="TimeLiveBI" #output table name
split_symbol="⋂" #split symbol for each level
BOIP=${BO%%:*}
BOPORT=${BO#*:}


start=$(date +'%s')

if [[ ${X_PARAMS//"a.age"/} != ${X_PARAMS} ]];then
	if [[ ${Y_PARAMS//"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt"/} != ${Y_PARAMS} ]];then
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"(select *,year(a.trans_date)-year(b3.birthday) as age from ${TABLE_PARAM%	*} a left join trans_head m on a.trans_id = m.trans_id left join VIP_member b3 on m.VIP_cardnum = b3.ID inner join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id) TABLES:${TABLE_PARAM%	*}"}
	else
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"(select *,year(a.trans_date)-year(b3.birthday) as age from ${TABLE_PARAM%	*} a left join trans_head m on a.trans_id = m.trans_id left join VIP_member b3 on m.VIP_cardnum = b3.ID) TABLES:${TABLE_PARAM%	*}"}
	fi
else
	if [[ ${Y_PARAMS//"m.VIP_cardnum"/} != ${Y_PARAMS} ]] || [[ -n ${TRACK_TICKET} ]];then
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"${TABLE_PARAM%	*} a left join trans_head m on a.trans_id = m.trans_id TABLES:${TABLE_PARAM%	*}"}
	fi
	
	if [[ ${Y_PARAMS//"sum(trans_price) as trans_price ,sum((p.product_cost+p.other_cost)*qty) as total_cost,avg(s.pings) as pings,sum(Oritrans_price-single_discount-total_discount) as origin_price,count(distinct trans_id) as cnt"/} != ${Y_PARAMS} ]];then
		TABLE_PARAM=${TABLE_PARAM//${TABLE_PARAM%	*}/"${TABLE_PARAM%	*} a inner join product p on a.product_id = p.product_id left join store s on a.store_id = s.store_id TABLES:${TABLE_PARAM%	*}"}
	fi 

fi
if [[ ${X_PARAMS//"ELS: "/} != ${X_PARAMS} ]] && [[ -n $CONDITIONS ]];then
	TABLE_PARAM=${TABLE_PARAM//" product "/" (select * from product filter by match(feature,'${CONDITIONS}')) "}
	CONDITIONS=""
fi

X_PARAMS=${X_PARAMS//ELS: /}

/webapp/query/TimeLiveBI  -ip $BOIP -port $BOPORT -CACHE $UseCache -PICK_TRACKER $PICK_TRACKER -EXCLUDE "$ResultExclude" -SHARE $ShareVariables -split_symbol "$split_symbol" -REMOTECLUSTER "$REMOTECLUSTER" -OUTPUTTABLE "$OUTPUTTABLE" -TABLE "$TABLE_PARAM" -X "$X_PARAMS" -Y "$Y_PARAMS" -SUBGROUP "$SUBGROUP_PARAM" -FILTER "$FILTER_PARAMS" -TRACK "$TRACK_TICKET" -TIME "$TIME_PARAMS" -CONDITIONS "$CONDITIONS" -POSTFIX "$POSTFIX"


#this is only needed if you want to generate your specific sql
if [[ ${ShareVariables} -eq 1 ]];then
	source ./share_var_$POSTFIX
	IFS='|' read -a FILTER_PARAMS <<< ${FILTER_PARAMS//	/|};
	if [[ -z $FILTER_PARAMS ]];then
		filtersize=1
	else
		filtersize=${#FILTER_PARAMS[@]}
	fi
	for (( j=0; j<$filtersize; j++ ))
	do	
		X_DIM=X_DIM$j
		Y_CAL=Y_CAL$j
		MainTable=MainTable$j
		MainTableFilter=MainTableFilter$j
		MainTableCondition=MainTableCondition$j
		TrackTable=TrackTable$j
		TimeSetting=TimeSetting$j
		JoinTablesFilter=JoinTablesFilter$j
		FilterLabel=FilterLabel$j
		SubgroupLabel=SubgroupLabel$j
		ResultFilter=ResultFilter$j
		GeneratedSQL=GeneratedSQL$j
  
		echo "X_DIM: ${!X_DIM}"
		echo "Y_CAL: ${!Y_CAL}"
		echo "MainTable ${!MainTable}" 
		echo "MainTableFilter ${!MainTableFilter}"
		echo "MainTableCondition ${!MainTableCondition}"
		echo "TrackTable ${!TrackTable}"
		echo "TimeSetting ${!TimeSetting}"
		echo "JoinTablesFilter ${!JoinTablesFilter}"
		echo "FilterLabel ${!FilterLabel}"
		echo "SubgroupLabel	${!SubgroupLabel}"
		echo "ResultFilter ${!ResultFilter}"
		echo "GeneratedSQL ${!GeneratedSQL}"
	
		SQL="select * from (cluster select ${!X_DIM} as x,${!Y_CAL} as y,${!FilterLabel} as filter, '${!SubgroupLabel}' as subgroup from (select * from ${!MainTable} a ${!TrackTable} ${!TimeSetting} ${!MainTableFilter} ${!MainTableCondition}) a ${!JoinTablesFilter} group by x,filter partition by type <> 'master') ${!ResultFilter}"
		echo "in bash $j: $SQL"

		 echo "select x,PositionLat,PositionLon from (cluster select ${!X_DIM} as x,PositionLat,PositionLon,${!FilterLabel} as filter from (select * from ${!MainTable} a ${!TrackTable} ${!TimeSetting} ${!MainTableFilter} ${!MainTableCondition}) a ${!JoinTablesFilter} group by x,filter partition by type <> 'master')"

	done
	
	
fi

file="./share_var_$POSTFIX"

if [ -f $file ] ; then
    rm $file
fi

# to limit the number of x passing to frontend ui
 # MYSQL="mysql -h $BOIP"
# checkparams=`$MYSQL -s -N -e "select count(*) from TimeLiveBI_$POSTFIX"`
# if [[ $checkparams -gt 1000 ]]; then
# 	$MYSQL -e "drop table TimeLiveBI_${POSTFIX}_temp" || true
# 	$MYSQL -e "rename table TimeLiveBI_$POSTFIX to TimeLiveBI_${POSTFIX}_temp"
# 	$MYSQL -e "create table TimeLiveBI_$POSTFIX as (select * from TimeLiveBI_${POSTFIX}_temp order by y desc limit 1000)"
# 	$MYSQL -e "drop table TimeLiveBI_${POSTFIX}_temp" || true
# fi    


echo "It took $(($(date +'%s') - $start)) seconds"
