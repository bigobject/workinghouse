# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 15:13:23 2020

@author: frank
"""
#set parameter py

import configparser
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime
import time
import mysql.connector
from mysql.connector import Error
import dplython as dplyr

def db_query(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='ubuntu' ,
                                     password='frank')
    sql_select_Query = sql_cmd
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    cursor.close()
    return records


def RFM(table,R = 'r',F = 'f',M = 'm',R_n = 5,F_n = 5,M_n = 5,R_max =100,F_max = 15,M_max = 3500, limit = 100000000):
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    plt.style.use('ggplot')
    rfm = table.copy()
    r_range = np.linspace(0,R_max, R_n)
    f_range = np.linspace(0,F_max, F_n)
    m_range = np.linspace(0,M_max, M_n)
    r_range = np.append(r_range, limit)
    f_range = np.append(f_range, limit)
    m_range = np.append(m_range, limit)
    r_class = sorted(list(range(1,R_n+1)),reverse=True)    
    f_class = list(range(1,F_n+1))
    m_class = list(range(1,M_n+1))
    
    rfm['R_socre'] = pd.cut(rfm[R],bins = r_range,labels =r_class,right = False).astype(float)
    rfm['F_socre'] = pd.cut(rfm[F],bins =  f_range,labels = f_class,right = False).astype(float)
    rfm['M_socre'] = pd.cut(rfm[M],bins =  m_range,labels =m_class,right = False).astype(float)
    plt.hist(rfm['R_socre']),plt.title('R_socre'),plt.show()
    plt.hist(rfm['F_socre']),plt.title('F_socre'),plt.show()
    plt.hist(rfm['M_socre']),plt.title('M_socre'),plt.show()    
    return rfm



def tagging(df,column = 'n_qty',n = 5,n_max =10, limit = 100000000):
    import matplotlib.pyplot as plt
    import pandas as pd
    import numpy as np
    tag = df.copy()
    n_range = np.linspace(0,n_max, n)
    n_range = np.append(n_range, limit)
    n_class = list(range(1,n+1))
    tag['tag'] = pd.cut(tag[column],bins = n_range,labels =n_class,right = False).astype(int)
    plt.hist(tag['tag'])
    return tag



def price_quantile(table,m_key = 'VIP_cardnum',key = 'categoryid_3nd',value = 'price',n_product = 5,quantile_list = [0.2,0.4,0.6,0.8],column_name = ['low','low_medium','medium','high'],score = [1,2,3,4,5]):
    import pandas as pd
    import numpy as np
    if len(quantile_list) != 4 or len(column_name) != 4 or len(score) !=5:
        print('Please Give 4 quantiles or 4 range_name or 5 score')
    else:        
        df = table.copy()
        name = column_name.copy()
        df[value] = df[value].astype(int)
        df = df[[key,value]].groupby(key).quantile(q=quantile_list)
        df['tmp'] = df.index
        df.reset_index(inplace=True,drop = True)
        df['key'] = 0 ; df['quantile'] = 0
        df['key'] = df['tmp'].map(lambda x:x[0] )
        df['quantile'] = df['tmp'].map(lambda x:x[1] )
       
        df1 = df[['key','quantile','price']].pivot(index='key', columns='quantile', values='price')
        df1.columns = name
        name.insert(0,'key')
        df1['key'] = df1.index
        df1.reset_index(inplace=True,drop = True)
        df1 = df1[name]
        df_mer = table.copy()
        df_mer[value] = df_mer[value].astype(int)
        
        df_tmp = df_mer[[m_key,key]].groupby(by=[m_key]).count()
        df_tmp[m_key] =df_tmp.index
        df_tmp = df_tmp[[m_key,key]]
        df_tmp.columns = [m_key,'cnt']
        df_mer = df_mer[df_mer[m_key].isin(df_tmp[df_tmp['cnt']>=n_product][m_key])]
       
        df_mer = df_mer.merge(df1, left_on=key, right_on='key')
        df_mer.loc[(df_mer['price']<=df_mer['low']),'score']=score[0]
        df_mer.loc[(df_mer['price']> df_mer['low']) & (df_mer['price']<=df_mer['low_medium']),'score']=score[1]
        df_mer.loc[(df_mer['price']> df_mer['low_medium']) & (df_mer['price']<=df_mer['medium']),'score']=score[2]
        df_mer.loc[(df_mer['price']> df_mer['medium']) & (df_mer['price']<=df_mer['high']),'score']=score[3]
        df_mer.loc[(df_mer['price']> df_mer['high']),'score']=score[4]
        df_mer = df_mer[[m_key,'score']].groupby(by=[m_key]).mean()
        df_mer[m_key] = df_mer.index
        df_mer = df_mer[[m_key,'score']]
        df_mer.reset_index(inplace=True,drop = True)
        
        df_mer.loc[(df_mer['score']<=score[0]),'score']=score[0]
        df_mer.loc[(df_mer['score']> score[0]) & (df_mer['score']<=score[1]),'score']=score[1]
        df_mer.loc[(df_mer['score']> score[1]) & (df_mer['score']<=score[2]),'score']=score[2]
        df_mer.loc[(df_mer['score']> score[2]) & (df_mer['score']<score[3]),'score']=score[3]
        df_mer.loc[(df_mer['score']> score[3]),'score']=score[4]
        
    return df_mer  



def top_category(table,key = 'VIP_cardnum',value = 'categoryid_3nd',n_product = 15,n_category = 2,class_category =5):
    df = table[[key,value]].copy()
    df1 = df.groupby(by=[key]).count()
    df1[key] = df1.index
    df1 = df1[[key,value]]
    df1.columns = [key,'cnt']
    df2 = df[df[key].isin(df1[df1['cnt']>=n_product][key])]
    df2['cnt'] = 0
    df2 = df2.groupby(by=[key,value]).count()
    df2['tmp'] = df2.index
    df2[key] = 0 ; df2[value] = 0
    df2.reset_index(inplace=True,drop = True)
    df2 = df2[df2['cnt']>=n_category]
    df2[key] = df2['tmp'].map(lambda x:x[0] )
    df2[value] = df2['tmp'].map(lambda x:x[1] )
    
    df2 = df2[[key,value,'cnt']]
    df2['Rank'] = df2.groupby(key)['cnt'].rank(ascending=False,method='first')
    df3 = df2[df2['Rank']<=class_category].sort_values(by=[key, 'Rank'])
    df3 = df3[[key,value,'Rank']].pivot(index=key, columns='Rank', values=value)
    column_name = list(df3.columns)
    column_name.insert(0,key)
    df3[key] = df3.index
    df3.reset_index(inplace=True,drop = True)
    df3 = df3[column_name]
    df3 = df3.fillna('')
    
    return df3

def user_behavior(table,m_key ='VIP_cardnum',is_time =False,key = 'store_id',time_for = 'weekday',cnt = 3 ,n = 2):
    if is_time == False:
        df1 = table[[m_key,key]].copy()
        df1['cnt'] = 0
        df2 = df1.groupby(by=[m_key,key]).count()
        df2['tmp'] = df2.index
        df2[m_key] = 0;df2[key] = 0
        df2 = df2[df2['cnt']>= cnt]
        df2[m_key] = df2['tmp'].map(lambda x:x[0] )
        df2[key] = df2['tmp'].map(lambda x:x[1] )
        '''
        for i in range(0,len(df2)):
            print(i)
            df2[m_key].iloc[i] = df2['tmp'].iloc[i][0]
            df2[key].iloc[i] = df2['tmp'].iloc[i][1]
        '''
        df2 = df2[[m_key,key,'cnt']]
        df2.reset_index(inplace=True,drop = True)
        df2['rank'] = df2.groupby(m_key)['cnt'].rank(ascending=False,method='first')
        df3 = df2.loc[df2['rank']<= n][[m_key,key,'cnt']]
        df3.reset_index(inplace=True,drop = True)
        
    else:
        if time_for == 'weekday':
            df1 = table.copy()
            df1['time_for'] = pd.to_datetime(df1[key]).dt.weekday
        else :
            df1 = table.copy()
            df1['time_for'] = pd.to_datetime(df1[key]).dt.hour
        df1 = df1[[m_key,'time_for']].copy()
        df1['cnt'] = 0
        df2 = df1.groupby(by=[m_key,'time_for']).count()
        df2['tmp'] = df2.index
        df2[m_key] = 0;df2['time_for'] = 0
        df2 = df2[df2['cnt']>= cnt]
        df2[m_key] = df2['tmp'].map(lambda x:x[0] )
        df2['time_for'] = df2['tmp'].map(lambda x:x[1] )
        '''
        for i in range(0,len(df2)):
            print(i)
            df2[m_key].iloc[i] = df2['tmp'].iloc[i][0]
            df2['time_for'].iloc[i] = df2['tmp'].iloc[i][1]
        '''
        df2 = df2[[m_key,'time_for','cnt']]
        df2.reset_index(inplace=True,drop = True)
        df2['rank'] = df2.groupby(m_key)['cnt'].rank(ascending=False,method='first')
        df3 = df2.loc[df2['rank']<=n][[m_key,'time_for','cnt']]
        df3.reset_index(inplace=True,drop = True)
        
    return df3

def trans_CV(table,m_key = 'VIP_cardnum',value = 'trans_price',n = 2,quantile_list = [0.2,0.4,0.6,0.8],score = [1,2,3,4,5]):
    if len(quantile_list) != 4  or len(score) !=5:
        print('Please Give 4 quantiles or 5 score')
    else:
        
        df = table.copy()
        df[value] = df[value].astype(int)
        df1 = df.groupby(m_key).count()
        df1[m_key] = df1.index
        df1.reset_index(inplace=True,drop = True)
        df1 = df1[[m_key,value]]
        df1.columns = [m_key,'cnt']
        df2 = df[df[m_key].isin(df1[df1['cnt']>=n][m_key])]

        df3= df2.groupby(m_key).mean()
        df3['std'] = df2.groupby(m_key).std()
        df3.columns = ['mean','std']
        df3[m_key] = df3.index
        df3.reset_index(inplace=True,drop = True)
        df3['CV'] = df3['mean']/df3['std']
        df3 = df3[[m_key,'CV']]
        df3['CV'][np.isinf(df3['CV'])] = 0
        df3 = df3.loc[(df3['CV']!= 0) ]
        df3['score'] = 0
        a = list(df3['CV'].quantile(q=quantile_list))

        df3.loc[(df3['CV']<=a[0]),'score']=score[0]
        df3.loc[(df3['CV']> a[0]) & (df3['CV']<=a[1]),'score']=score[1]
        df3.loc[(df3['CV']> a[1]) & (df3['CV']<=a[2]),'score']=score[2]
        df3.loc[(df3['CV']> a[2]) & (df3['CV']<=a[3]),'score']=score[3]
        df3.loc[(df3['CV']> a[3]),'score']=score[4]
        df3  = df3[df3['score']!=0]
        plt.hist(df3['score'])
    return df3

def db_query(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='1111',
                                     database='bigobject',
                                     user='ubuntu' ,
                                     password='frank')
    sql_select_Query = sql_cmd
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    cursor.close()
    return records

def db_query_exec(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='1111',
                                     database='bigobject',
                                     user='ubuntu' ,
                                     password='frank')
    cursor = connection.cursor()
    cursor.execute(sql_cmd)
    cursor.close()
    
def db_query_load(sql_cmd,list_data):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='1111',
                                     database='bigobject',
                                     user='ubuntu' ,
                                     password='frank')
    cursor = connection.cursor()
    cursor.executemany(sql_cmd, list_data)
    cursor.close()



'''
def main():
     if sys.argv[1]=="RFM":
       print("=======================")
       #print("nrec=10 only")
       #print("FREQ>5 2018-2019 train, 3 month test")
       RFM_table = RFM(table, R = 'r',F = 'f',M = 'm',R_n = 5,F_n = 5,M_n = 5,R_max =100,F_max = 15,M_max = 3500, limit = 100000000)
    elif sys.argv[1]=="Tagging":
       test2 = tagging(df,'credit_per',3,1)
    
if __name__=="__main__":
    main()
'''