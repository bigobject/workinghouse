import mysql.connector
import pandas as pd 
import numpy as np
from scipy import sparse

def db_query(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='frank' ,
                                     password='frank')
    sql_select_Query = sql_cmd
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    cursor.close()
    return records


def db_query_exec(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='frank' ,
                                     password='frank')
    sql_select_Query = sql_cmd
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    cursor.close()

def preprocess(source_table="trans_prod_asso"):
    df = db_query("select trans_id,product_name,1 as value from "+source_table)
    df=pd.DataFrame(df)
    df.columns=["trans_id","product_name","value"]
    onehot=pd.pivot_table(df, index = 'trans_id', values = 'value', columns = 'product_name', aggfunc = 'sum',fill_value=0)
    print("transaction count:{}, product count:{}".format(onehot.shape[0],onehot.shape[1]))
    sparse_onehot=sparse.csr_matrix(onehot) 
    col_name=onehot.columns
    del onehot
    sparse_onehot_df = pd.DataFrame.sparse.from_spmatrix(sparse_onehot, columns=col_name)
    return sparse_onehot_df

    
def save_all_rules(rules,first_time=True):
    if first_time==True:
        rules["antecedents"] = rules["antecedents"].apply(lambda x: ','.join(list(x))).astype("unicode")
        rules["consequents"] = rules["consequents"].apply(lambda x: ','.join(list(x))).astype("unicode")
        bo_table="association_rules"
    else:
        bo_table="strict_rules"
    try:
        col_schema1="'"+rules.columns[0]+"' VARSTRING(200),'"+ rules.columns[1]+"' VARSTRING(200),"
        col_schema2=', '.join(["'"+col+"' FLOAT" for col in list(rules.columns)[2:]])
        try:
            db_query_exec("drop table "+bo_table)
        except:
            pass 
        db_query_exec("create table "+bo_table +" ("+col_schema1+col_schema2+")")
        sql = "INSERT INTO "+bo_table +"('antecedents', 'consequents', 'antecedent support','consequent support', 'support', 'confidence', 'lift', 'leverage','conviction') VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        connection = mysql.connector.connect(host='127.0.0.1' ,   
                                    port='13301',
                                database='bigobject',
                                user='frank',password='frank')
        cursor = connection.cursor()
        dataset=[tuple(i) for _,i in rules.iterrows()]
        cursor.executemany(sql,dataset)
        cursor.close()
    except:
        print("fail to save results to bo table")
