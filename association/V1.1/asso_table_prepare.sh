#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
MYSQL="mysql -h 127.0.0.1 -P 13301"
start_time="2019-01-01"
end_time="2019-03-01"
#end_time="2019-02-01"
echo "=create filter products' table="
time $MYSQL -e "drop table filter_prod_id;"|| true
time $MYSQL -e "create table filter_prod_id as (select product_id,b.id,product_name from product a inner join 2nd_category b on a.categoryid_2nd=b.categoryid_2nd where product_name like '%會員申辦%' or product_name like '%背心袋' or product_name like '%折價券%' or product_name like '%贈品%' ) key(product_id);"|| true

echo "=create transactions-products table="
time $MYSQL -e "drop table trans_prod_asso;"|| true
time $MYSQL -e "create table trans_prod_asso as (select trans_id,f.product_name as product_name from (select trans_id,product_id from (select * from trans_record where trans_datetime>=\"${start_time}\" and trans_datetime<=\"${end_time}\" and not in filter_prod_id) c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${start_time}\" and trans_datetime<=\"${end_time}\" ) a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id ) e inner join product f on e.product_id=f.product_id group by trans_id,product_name);"|| true
