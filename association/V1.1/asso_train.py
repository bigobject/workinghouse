import utils
import pandas as pd
from scipy import sparse
import mysql.connector
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
from time import time

def main():
    start = time()
    sparse_onehot_df = utils.preprocess(source_table="trans_prod_asso")
    # Compute frequent itemsets using the Apriori algorithm
    frequent_itemsets = apriori(sparse_onehot_df, 
                                min_support = 0.0001, 
                                max_len = 3, 
                                use_colnames = True, verbose=1,low_memory=True) #low_memory=False faster
    
    print("frequent item sets count:{}".format(len(frequent_itemsets)))
    
    #find and save all association rules for frequent_itemsets
    rules = association_rules(frequent_itemsets, 
                                metric = "support", 
                                min_threshold = 0.00001)
    print("Total rules count:{}".format(rules.shape[0]))
    utils.save_all_rules(rules,first_time=True)
    
    #find and save strict rules
    strict_rules={'lift':1.5,'confidence':0.5,'support':0.0005} #,'conviction':1.0
    for metric,thres in strict_rules.items():
        rules = rules[rules[metric] > thres]
    print("Total srtict rules count:{}".format(rules.shape[0]))   
    utils.save_all_rules(rules,first_time=False)
    end = time()
    print("Time cost:{} secs.".format(end-start))



if __name__=="__main__":
    main()