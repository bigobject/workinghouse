#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
MYSQL="mysql -h 127.0.0.1 -P 13301" 
Training_start="2018-11-20"
Training_end="2019-05-20 23:59:59"
Testing_start="2019-05-20 23:59:59"
Testing_end="2019-08-20 23:59:59"
#Testing_end="2019-06-20 23:59:59"
buy_freq_threshold="1"
prod_freq_threshold="30"

echo "create filter products' table"
$MYSQL -e "drop table filter_prod_id_rec;"|| true
$MYSQL -e "create table filter_prod_id_rec as (select product_id,b.id,product_name from product a inner join 2nd_category b on a.categoryid_2nd=b.categoryid_2nd where product_name like '%會員申辦%' or product_name like '%背心袋' or product_name like '%折價券%' or product_name like '%贈品%' ) key(product_id);"|| true

##Filter frequent user and frequent product by threshold
$MYSQL -e "drop table user_buyitem_cnt;"|| true 
$MYSQL -e "create table user_buyitem_cnt as (select d.VIP_cardnum as VIP_cardnum,count(distinct product_id) as item_cnt,count(distinct trans_id) as buy_cnt,sum(qty) as buy_qty from trans_record c inner join(select trans_id,VIP_cardnum from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${Training_start}\"  and trans_datetime<=\"${Training_end}\") a inner join (select * from VIP_member where VIP_CARD<>'') b on a.VIP_cardnum=b.VIP_CARD group by trans_id,VIP_cardnum) d  on c.trans_id=d.trans_id group by VIP_cardnum);"|| true  
echo "user_buyitem_cnt done"
$MYSQL -e "drop table item_user_cnt;"|| true 
$MYSQL -e "create table item_user_cnt as (select product_id ,count(distinct d.VIP_cardnum) as user_cnt,count(distinct trans_id) as sale_cnt from trans_record c  inner join (select * from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") a inner join (select * from VIP_member where VIP_CARD<>'') b on a.VIP_cardnum=b.VIP_CARD) d  on c.trans_id=d.trans_id group by product_id);"|| true 
echo "item_user_cnt done"
$MYSQL -e "drop table freq_member;"|| true  
$MYSQL -e "create table freq_member as (select VIP_cardnum from user_buyitem_cnt where buy_cnt>${buy_freq_threshold}) key(VIP_cardnum);"|| true  
$MYSQL -e "drop table frequent_prod;"|| true  
$MYSQL -e "create table frequent_prod as (select product_id from item_user_cnt where sale_cnt>${prod_freq_threshold} and not in filter_prod_id_rec) key(product_id);"|| true

##create user-product traning and testing data 
$MYSQL -e "drop table user_prod_train;"|| true  
$MYSQL -e "create table user_prod_train as (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where in frequent_prod and trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and in freq_member and trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id);"|| true 
echo "user_prod_train done"
$MYSQL -e "drop table user_prod_test;"|| true  
$MYSQL -e "create table user_prod_test as (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where in frequent_prod and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and in freq_member and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id);"|| true 

echo "user_prod_test done"
##find popular products during trainiuser_prod_trainng time 
$MYSQL -e "drop table popular_items;"|| true 
$MYSQL -e "create table popular_items as (select product_id,count(*) as cnt from user_prod_train group by product_id order by cnt desc limit 100)"|| true 

##prepare table for other factors (Ex. sex and categories)
$MYSQL -e "drop table train_member"|| true 
$MYSQL -e "create table train_member as (select VIP_cardnum from user_prod_train group by VIP_cardnum) key(VIP_cardnum); "|| true 
$MYSQL -e "drop table user_sex_train;"|| true 
$MYSQL -e "create table user_sex_train as (select VIP_cardnum,sex from (select VIP_CARD as VIP_cardnum,sex from VIP_member) where in train_member group by VIP_cardnum,sex) key(VIP_cardnum);"|| true 
$MYSQL -e "drop table user_cat_train;"|| true
$MYSQL -e "create table user_cat_train as (select VIP_cardnum,d.categoryname_2nd as categoryname_2nd,d.categoryid_2nd as categoryid_2nd,count(*) as cnt,sum(d.qty) as sum_qty from (select trans_id,VIP_cardnum from trans_head where in freq_member and  trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") c inner join (select trans_id,product_id,b.categoryid_2nd as categoryid_2nd,b.categoryname_2nd as categoryname_2nd,qty from (select * from trans_record  where in frequent_prod and  trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") a inner join (select product_id,2nd_category.categoryid_2nd as categoryid_2nd,2nd_category.categoryname_2nd as categoryname_2nd,2nd_category.id from product inner join 2nd_category on product.categoryid_1nd=2nd_category.categoryid_1nd and product.categoryid_2nd=2nd_category.categoryid_2nd) b on a.product_id=b.product_id) d on c.trans_id=d.trans_id group by VIP_cardnum,categoryname_2nd);"|| true

###############################################################################
#Set up table for recommendation(not for training and testing)
$MYSQL -e "drop table user_prod;"|| true  
$MYSQL -e "create table user_prod as (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where in frequent_prod and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and in freq_member and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id);"|| true 
echo "create user_prod table for recommendation done"

#echo "Amount of Total Users and percentage without filtering:"
#$MYSQL -e "drop table total_users"|| true 
#$MYSQL -e "create table total_users as (select count(distinct VIP_cardnum) as total_users from (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where in frequent_prod and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and in freq_member and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id))"|| true 
$MYSQL -e "drop table total_users"|| true 
$MYSQL -e "create table total_users as (select count(distinct VIP_cardnum) as total_users from (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id))"|| true 


#time $MYSQL -e "drop table user_prod_pivot"|| true
#time $MYSQL -e "create table user_prod_pivot as (SELECT * FROM user_prod PIVOT (VIP_cardnum AS user_id, SUM(1) FOR product_id IN (*)));"|| true 

#idf table 
#time $MYSQL -e "drop table product_cnt_idf"|| true
#time $MYSQL -e "create table product_cnt_idf as (select product_id,log(17418/cnt*1.0) as idf from (select product_id,count(*) as cnt from trans_record group by product_id));"|| true
