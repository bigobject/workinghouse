﻿
import mysql.connector
import pandas as pd 
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from scipy.spatial.distance import cosine
from scipy.spatial.distance import jaccard
from scipy.spatial.distance import cdist

host='127.0.0.1'
port='13301'
database='bigobject'
user='frank'
password='frank'

def db_query(sql_cmd):
    connection = mysql.connector.connect(host=host ,   
                                        port=port,
                                     database=database,
                                     user=user ,
                                     password=password)
    cursor = connection.cursor()
    cursor.execute(sql_cmd)
    records = cursor.fetchall()
    cursor.close()
    return records


def db_query_exec(sql_cmd):
    connection = mysql.connector.connect(host=host ,   
                                        port=port,
                                     database=database,
                                     user=user ,
                                     password=password)
    cursor = connection.cursor()
    cursor.execute(sql_cmd)
    cursor.close()

def db_query_exec_many(sql_cmd):
    sql_select_Query = sql_cmd
    connection = mysql.connector.connect(host=host ,   
                                        port=port,
                                     database=database,
                                     user=user ,
                                     password=password)
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    cursor.close()


def create_user_item_matrix(table="user_prod_train",type="frequency"):
    if type=="onehot":
        sql_select_Query = "select VIP_cardnum,product_id,1 as cnt from bigobject."+table+" group by VIP_cardnum,product_id"
    elif type=="frequency":
        sql_select_Query = "select VIP_cardnum,product_id,count(*) as cnt from bigobject."+table +" group by VIP_cardnum,product_id"
    elif type=="self_define":
        sql_select_Query = "select VIP_cardnum,categoryname_2nd,cnt from user_cat_train"
    records=db_query(sql_select_Query)
    user_prod_df=pd.DataFrame(records,columns=["user_id","prod_id","cnt"])
    user_prod_matrix = user_prod_df.pivot_table(index='user_id', columns='prod_id', values='cnt')
    #print("user count, "+str(user_prod_matrix.shape[0])+", product count "+str(user_prod_matrix.shape[1]))
    user_prod_matrix=user_prod_matrix.fillna(0)
    return user_prod_df,user_prod_matrix

def create_similarity_df(user_prod_matrix_train_norm,k_users,all_users):
    #all_users=user_prod_matrix_train_norm.index
    all_sim_df=pd.DataFrame()
    for user_id in all_users:
        #print(user_id) #sim=cosine_similarity(np.array(user_prod_matrix_train_norm.loc[user_prod_matrix_train_norm.index==user_id]).reshape(1,-1),user_prod_matrix_train_norm)[0]
        sim=cosine_similarity(user_prod_matrix_train_norm.getrow(list(all_users).index(user_id)),user_prod_matrix_train_norm)[0]
        sim_order=sim.argsort()[::-1][1:k_users+1]        
        topn_sim=sim[sim_order]
        topn_user=all_users[sim_order]
        sim_df=pd.DataFrame({"topn_neighbor":list(topn_user),"similarity":list(topn_sim)}) 
        #sim_df.loc[sim_df['similarity']>]
        sim_df["Userid"]=user_id
        all_sim_df=all_sim_df.append(sim_df)
    return all_sim_df


def recommend(user_prod_matrix_train,all_sim_df,user_i,n_rec=20,prevent_duplicated=False):    
    if prevent_duplicated==True: 
        all_prod=user_prod_matrix_train[user_prod_matrix_train.index==user_i]
        puchased_prod=all_prod.T[(all_prod.T>0).values.flatten()].index
        user_sim_df=all_sim_df.loc[all_sim_df.index==user_i]
        recommend_order=user_sim_df.apply(lambda x:x.iloc[2:]*x["similarity"],axis=1).sum().sort_values(ascending=False)
        recommended=recommend_order[(recommend_order.index.isin(puchased_prod)==False)&(recommend_order>0)][:n_rec]        
    else:
        user_sim_df=all_sim_df.loc[all_sim_df.index==user_i]
        recommended=user_sim_df.apply(lambda x:x.iloc[2:]*x["similarity"],axis=1).sum().sort_values(ascending=False)[:n_rec]
    return recommended

#def recommend_popular(n_rec=10):
#    pop_items=popular_items[:n_rec]
#    return pop_items
#
#def test_recommend_evaluation_popular(train_data,test_data,similarities_sparse,topn_neighbors,popular_items,n_rec):
#    all_users=user_prod_matrix_test.index.unique().values
#    precision_all={}
#    recall_all={}
#    all_test_recommend=set()
#    popular_sum=0
#    rec_count=0
#    #n_items=10
#    for user_id in all_users:
#        try: 
#            #real purchase 
#            test=user_prod_matrix_test.loc[user_prod_matrix_test.index==user_id].apply(lambda x:set(x[x>0].index.values),axis=1).values[0]
#            #recommended
#            test_recommend=set(recommend_popular(n_rec))
#            for i in test_recommend:
#                all_test_recommend.add(i)
#                if i in popular_items:
#                    popular_sum+=1
#                rec_count+=1
#            hit=len(test.intersection(test_recommend))
#            print(hit)
#            real_purchase=len(test)            #real_purchase=np.sum(user_prod_matrix.loc[user_prod_matrix.index==user_id].values)    
#            precision_all[user_id]=hit/n_rec*1.0
#            recall_all[user_id]=hit/real_purchase*1.0
#        except:
#            pass
#    mean_precision=np.mean(list(precision_all.values()))
#    mean_recall=np.mean(list(recall_all.values()))
#    coverage = len(all_test_recommend) / (1.0 * user_prod_matrix_test.shape[1])   
#    popularity = popular_sum / (n_rec*len(all_users)*1.0) #user_prod_m_train.shape[0]
#    print("Precision:"+str(mean_precision))
#    print("Recall:"+str(mean_recall))
#    print("Coverage:"+str(coverage))
#    print("Popularity:"+str(popularity))


