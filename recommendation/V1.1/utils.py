
import mysql.connector
import pandas as pd 
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from scipy.spatial.distance import cosine
from scipy.spatial.distance import jaccard
from scipy.spatial.distance import cdist

def db_query(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='ubuntu' ,
                                     password='frank')
    sql_select_Query = sql_cmd
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    cursor.close()
    return records


def db_query_exec(sql_cmd):
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='ubuntu' ,
                                     password='frank')
    sql_select_Query = sql_cmd
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    cursor.close()

def create_user_item_matrix(table="user_prod_train",type="frequency"):
    if type=="onehot":
        sql_select_Query = "select *,1 as cnt from bigobject."+table+" group by VIP_cardnum,product_id"
    elif type=="frequency":
        sql_select_Query = "select VIP_cardnum,product_id,count(*) as cnt from bigobject."+table +" group by VIP_cardnum,product_id"
    elif type=="wieghted_freq":
        sql_select_Query = "select VIP_cardnum,product_id,a.cnt*b.idf as weighted_cnt from (select VIP_cardnum,product_id,count(*) as cnt from bigobject."+table +" group by VIP_cardnum,product_id) a inner join product_cnt_idf b on a.product_id=b.product_id where not VIP_cardnum like '[0123456789]%'"
        # create table product_cnt_idf as (select product_id,log(17418/cnt*1.0) as idf from (select product_id,count(*) as cnt from trans_record group by product_id));
    elif type=="wieghted_onehot":
        sql_select_Query = "select VIP_cardnum,product_id,a.cnt*b.idf as weighted_cnt from (select VIP_cardnum,product_id,1 as cnt from bigobject."+table +" group by VIP_cardnum,product_id) a inner join product_cnt_idf b on a.product_id=b.product_id where not VIP_cardnum like '[0123456789]%'"
    elif type=="self_define":
        sql_select_Query = "select VIP_cardnum,categoryname_2nd,cnt from user_cat_train"
    records=db_query(sql_select_Query)
    user_prod_df=pd.DataFrame(records,columns=["user_id","prod_id","cnt"])
    #print("user_prod_df shape , "+str(user_prod_df.shape))
    user_prod_matrix = user_prod_df.pivot_table(index='user_id', columns='prod_id', values='cnt')
    print("user count, "+str(user_prod_matrix.shape[0])+", product count "+str(user_prod_matrix.shape[1]))#user_prod_matrix_sampled=user_prod_matrix.sample(int(user_prod_matrix.shape[0]*0.2))
    user_prod_matrix=user_prod_matrix.fillna(0)
    return user_prod_df,user_prod_matrix



def find_topn_neighbors(user_prod_matrix_train,similarities_sparse,n=50):
    topn_list=list()
    for i in range(similarities_sparse.shape[0]):
        topn_by_row=pd.DataFrame(user_prod_matrix_train.index.values[np.argsort(similarities_sparse.getrow(i).toarray())[0][::-1][1:n+1]])
        topn_list.append(topn_by_row)
    topn_neighbors=pd.concat(topn_list,axis=1).T
    topn_neighbors.columns=['top{}'.format(i) for i in range(1, n+1)]
    topn_neighbors.index=user_prod_matrix_train.index.values
    return topn_neighbors

def recommend(user_prod_matrix_train,similarities_sparse,topn_neighbors,user_i,n_rec=20):
    '''
    recommend products and show weight for specified user 
    ex.'C220599325'
    '''
    neighbor_list=topn_neighbors.loc[topn_neighbors.index==user_i].values[0]
    neighbor_buy_list=user_prod_matrix_train.loc[user_prod_matrix_train.index.isin(neighbor_list)]
    #user_item_weighted=neighbor_buy_list.apply(lambda x:x*cosine_sim_df.loc[(topn_neighbors.index==user_i),x.name][0],axis=1)
    user_item_weighted=neighbor_buy_list.apply(lambda x:x*similarities_sparse.getrow(np.where(topn_neighbors.index==user_i)[0][0]).toarray()[0][np.where(topn_neighbors.index==x.name)[0][0]],axis=1)
    recommended=user_item_weighted.sum().sort_values(ascending=False)[:n_rec]
    return recommended

def test_recommend_evaluation(user_prod_matrix_train_norm,user_prod_matrix_test,similarities_sparse,topn_neighbors,popular_items,n_rec,combine_var_cnt):
    user_prod_matrix_test_filter=user_prod_matrix_test[user_prod_matrix_test.index.isin(user_prod_matrix_train_norm.index)]
    print('Amount of prod used for recommendation')
    print(user_prod_matrix_test_filter.shape)
    all_users=user_prod_matrix_test_filter.index.unique().values
    precision_all={}
    recall_all={}
    all_test_recommend=set()
    popular_sum=0
    rec_count=0
    user_prod_matrix_train_norm=user_prod_matrix_train_norm.iloc[:,:-combine_var_cnt]
    for user_id in all_users:
    #try:
        #real purchase 
        test=user_prod_matrix_test_filter.loc[user_prod_matrix_test_filter.index==user_id].apply(lambda x:set(x[x>0].index.values),axis=1).values[0]
        #recommended
        test_recommend=set(recommend(user_prod_matrix_train_norm,similarities_sparse,topn_neighbors,user_id,n_rec).index.values)
        for i in test_recommend:
            all_test_recommend.add(i)
            if i in popular_items:
                popular_sum+=1
            rec_count+=1
        hit=len(test.intersection(test_recommend))
        #print(hit)
        real_purchase=len(test)            
        precision_all[user_id]=hit/n_rec*1.0
        recall_all[user_id]=hit/real_purchase*1.0

    mean_precision=np.mean(list(precision_all.values()))
    mean_recall=np.mean(list(recall_all.values()))
    coverage = len(all_test_recommend) / (1.0 * user_prod_matrix_train_norm.shape[1])   
    popularity = popular_sum / (n_rec*len(all_users)*1.0) #user_prod_m_train.shape[0]
    print("Precision:"+str(mean_precision))
    print("Recall:"+str(mean_recall))
    print("Coverage:"+str(coverage))
    print("Popularity:"+str(popularity))

#def recommend_popular(n_rec=10):
#    pop_items=popular_items[:n_rec]
#    return pop_items
#
#def test_recommend_evaluation_popular(train_data,test_data,similarities_sparse,topn_neighbors,popular_items,n_rec):
#    all_users=user_prod_matrix_test.index.unique().values
#    precision_all={}
#    recall_all={}
#    all_test_recommend=set()
#    popular_sum=0
#    rec_count=0
#    #n_items=10
#    for user_id in all_users:
#        try: 
#            #real purchase 
#            test=user_prod_matrix_test.loc[user_prod_matrix_test.index==user_id].apply(lambda x:set(x[x>0].index.values),axis=1).values[0]
#            #recommended
#            test_recommend=set(recommend_popular(n_rec))
#            for i in test_recommend:
#                all_test_recommend.add(i)
#                if i in popular_items:
#                    popular_sum+=1
#                rec_count+=1
#            hit=len(test.intersection(test_recommend))
#            print(hit)
#            real_purchase=len(test)            #real_purchase=np.sum(user_prod_matrix.loc[user_prod_matrix.index==user_id].values)    
#            precision_all[user_id]=hit/n_rec*1.0
#            recall_all[user_id]=hit/real_purchase*1.0
#        except:
#            pass
#    mean_precision=np.mean(list(precision_all.values()))
#    mean_recall=np.mean(list(recall_all.values()))
#    coverage = len(all_test_recommend) / (1.0 * user_prod_matrix_test.shape[1])   
#    popularity = popular_sum / (n_rec*len(all_users)*1.0) #user_prod_m_train.shape[0]
#    print("Precision:"+str(mean_precision))
#    print("Recall:"+str(mean_recall))
#    print("Coverage:"+str(coverage))
#    print("Popularity:"+str(popularity))