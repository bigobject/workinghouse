#!/bin/bash
set -e
PATH=$PATH:/usr/local/bin:/usr/bin:/bin
MYSQL="mysql -h 127.0.0.1 -P 13301" 
#Training_start="2018-05-20"
Training_start="2018-11-20"
Training_end="2019-05-20 23:59:59"
Testing_start="2019-05-21"
Testing_end="2019-08-20 23:59:59"

buy_freq_threshold="10"
#buy_freq_threshold="5"
prod_freq_threshold="30"

#filter training time range (buying count>? in this time range)
time $MYSQL -e "drop table user_buyitem_cnt;"|| true 
time $MYSQL -e "create table user_buyitem_cnt as (select d.VIP_cardnum as VIP_cardnum,count(distinct product_id) as item_cnt,count(distinct trans_id) as buy_cnt,sum(qty) as buy_qty from trans_record c inner join(select trans_id,VIP_cardnum from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${Training_start}\"  and trans_datetime<=\"${Training_end}\") a inner join (select * from VIP_member where VIP_CARD<>'') b on a.VIP_cardnum=b.VIP_CARD group by trans_id,VIP_cardnum) d  on c.trans_id=d.trans_id group by VIP_cardnum);"|| true  
echo "user_buyitem_cnt done"

time $MYSQL -e "drop table item_user_cnt;"|| true 
time $MYSQL -e "create table item_user_cnt as (select product_id ,count(distinct d.VIP_cardnum) as user_cnt,count(distinct trans_id) as sale_cnt from trans_record c  inner join (select * from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") a inner join (select * from VIP_member where VIP_CARD<>'') b on a.VIP_cardnum=b.VIP_CARD) d  on c.trans_id=d.trans_id group by product_id);"|| true 
echo "item_user_cnt done"

#filter tables [change to certain time range???] 
time $MYSQL -e "drop table freq_member;"|| true  
time $MYSQL -e "create table freq_member as (select VIP_cardnum from user_buyitem_cnt where item_cnt>${buy_freq_threshold}) key(VIP_cardnum);"|| true  
time $MYSQL -e "select count(distinct VIP_cardnum) from freq_member;"|| true  
echo "freq_member done"

time $MYSQL -e "drop table frequent_prod;"|| true  
time $MYSQL -e "create table frequent_prod as (select product_id from item_user_cnt where user_cnt>${prod_freq_threshold}) key(product_id);"|| true
echo "frequent_prod done"

#user product 
time $MYSQL -e "drop table user_prod;"|| true  #add trans_datetime
time $MYSQL -e "create table user_prod as (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where in frequent_prod and trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Testing_end}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and in freq_member and trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Testing_end}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id);"|| true  
echo "user_prod done"


time $MYSQL -e "drop table user_prod_train;"|| true  
time $MYSQL -e "create table user_prod_train as (select VIP_cardnum,product_id from user_prod where trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\" );"|| true 
echo "user_prod_train done"

time $MYSQL -e "drop table user_prod_test;"|| true  
time $MYSQL -e "create table user_prod_test as (select VIP_cardnum,product_id from user_prod where trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_end}\" );"|| true 
echo "user_prod_test done"

#popular 
#popular_items=user_prod_df.groupby("prod_id").count()["user_id"].sort_values(ascending=False).index[:100]
time $MYSQL -e "drop table popular_items;"|| true 
time $MYSQL -e "create table popular_items as (select product_id,count(*) as cnt from user_prod_train group by product_id order by cnt desc limit 100)"|| true 

# 改從sql read(原在 db)
echo "training User Count"
time $MYSQL -e "select count(distinct VIP_cardnum) from user_prod_train"|| true 
echo "testing User Count"
time $MYSQL -e "select count(distinct VIP_cardnum) from user_prod_test"|| true 


time $MYSQL -e "drop table train_member"|| true 
time $MYSQL -e "create table train_member as (select VIP_cardnum from user_prod_train group by VIP_cardnum) key(VIP_cardnum); "|| true 

##other factors 
time $MYSQL -e "drop table user_sex_train;"|| true 
time $MYSQL -e "create table user_sex_train as (select VIP_cardnum,sex from (select VIP_CARD as VIP_cardnum,sex from VIP_member) where in train_member group by VIP_cardnum,sex) key(VIP_cardnum);"|| true 

time $MYSQL -e "drop table user_cat_train;"|| true
time $MYSQL -e "create table user_cat_train as (select VIP_cardnum,d.categoryname_2nd as categoryname_2nd,d.categoryid_2nd as categoryid_2nd,count(*) as cnt,sum(d.qty) as sum_qty from (select trans_id,VIP_cardnum from trans_head where in freq_member and  trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") c inner join (select trans_id,product_id,b.categoryid_2nd as categoryid_2nd,b.categoryname_2nd as categoryname_2nd,qty from (select * from trans_record  where in frequent_prod and  trans_datetime>=\"${Training_start}\" and trans_datetime<=\"${Training_end}\") a inner join (select product_id,2nd_category.categoryid_2nd as categoryid_2nd,2nd_category.categoryname_2nd as categoryname_2nd,2nd_category.id from product inner join 2nd_category on product.categoryid_1nd=2nd_category.categoryid_1nd and product.categoryid_2nd=2nd_category.categoryid_2nd) b on a.product_id=b.product_id) d on c.trans_id=d.trans_id group by VIP_cardnum,categoryname_2nd);"|| true

##save user_prod pivot for recommendation 
time $MYSQL -e "drop table user_prod_pivot"|| true
time $MYSQL -e "create table user_prod_pivot as (SELECT * FROM user_prod PIVOT (VIP_cardnum AS user_id, SUM(1) FOR product_id IN (*)) );"|| true 

#idf table 
time $MYSQL -e "drop table product_cnt_idf"|| true
time $MYSQL -e "create table product_cnt_idf as (select product_id,log(17418/cnt*1.0) as idf from (select product_id,count(*) as cnt from trans_record group by product_id));"|| true

echo "total user cnt for testing period"
$MYSQL -e "select count(distinct VIP_cardnum) from (select d.VIP_cardnum as VIP_cardnum,product_id,d.trans_datetime as trans_datetime from (select * from trans_record where trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_start}\") c inner join (select * from (select * from trans_head where VIP_cardnum<>'' and trans_datetime>=\"${Testing_start}\" and trans_datetime<=\"${Testing_start}\")  a inner join VIP_member b on a.VIP_cardnum=b.VIP_CARD) d on c.trans_id=d.trans_id);"||true

#testing data time range 多少比例的人被推薦到
#mysql> select 4268/57756.0;
#+--------------+
#| 4268/57756.0 |
#+--------------+
#|     0.073897 |
#+--------------+
