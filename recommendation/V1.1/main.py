﻿import User_CF
import sys
import utils
import pandas as pd 
import numpy as np
import mysql.connector

def run_evaluation_model(k_users=20,n_rec=10,weight_type="frequency",train_set="user_prod_train",test_set="user_prod_test",retrain=True,save_model=True):
    usercf_model=User_CF.User_CF(k_users,n_rec,weight_type,train_set,test_set,retrain,save_model)
    combine_matrix,user_prod_matrix_test,combine_var_cnt=usercf_model.preprocess()
    similarities_sparse,topn_neighbors=usercf_model.fit(combine_matrix)
    popular_items=usercf_model.find_popular_items()
    usercf_model.recommend_test(combine_matrix,user_prod_matrix_test,similarities_sparse,topn_neighbors,popular_items,combine_var_cnt)
    

def save_recommendation(table="user_prod_pivot",n_rec=20): #user_i="B220251169" #'K221992176'
    user_prod_pivot=pd.DataFrame(utils.db_query("select * from " +table))
    user_prod_pivot.columns=[i[0] for i in utils.db_query("desc  user_prod_pivot ")]
    user_prod_pivot.set_index('user_id',inplace=True)
    user_prod_pivot=user_prod_pivot.apply(lambda x:(x-np.min(x))/(np.max(x)-np.min(x)),axis=0)
    usercf_model=User_CF.User_CF(retrain=False)
    similarities_sparse,topn_neighbors=usercf_model.fit('')
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='ubuntu')
    cursor = connection.cursor()
    try:
        utils.db_query_exec("drop table saved_recommendation") 
    except:
        pass 
    utils.db_query_exec("create table saved_recommendation ('user_id' STRING, 'prod_id' STRING,'weight' FLOAT)")
    sql = "INSERT INTO saved_recommendation VALUES (%s, %s, %s)"    
    for user_id in user_prod_pivot.index:
        rec=utils.recommend(user_prod_pivot,similarities_sparse,topn_neighbors,user_id,n_rec)
        #rec='\''+'\',\''.join(list(rec.index.values))+'\''    
        val=[(user_id,rec.index[i],rec[i]) for i in range(len(rec))]
        cursor.executemany(sql, val)
    cursor.close()
    

def main():
    k_users=50
    n_rec=10
    weight_type="frequency" #frequency/onehot/wieghted_freq
    train_set="user_prod_train"
    test_set="user_prod_test"
    retrain=True
    save_model=True
    if sys.argv[1]=="evaluate":
       print("=======================")
       #print("nrec=10 only")
       #print("FREQ>5 2018-2019 train, 3 month test")
       run_evaluation_model(k_users=k_users,n_rec=n_rec,weight_type=weight_type,train_set=train_set,test_set=test_set,retrain=retrain,save_model=save_model)
    elif sys.argv[1]=="save_recommendation":
       save_recommendation(table="user_prod_pivot",n_rec=n_rec)
    elif sys.argv[1]=="recommend":
        print(sys.argv[2])
        try:
            rec=utils.db_query("select prod_id from saved_recommendation where user_id='"+sys.argv[2]+"'") 
            print(rec)
        except:
            print("no recommendation")

if __name__=="__main__":
    main()