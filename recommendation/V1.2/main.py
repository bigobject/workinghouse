﻿import User_CF
import sys
import utils
import pandas as pd 
import numpy as np
import mysql.connector

def run_evaluation_model(k_users=20,n_rec=10,weight_type="frequency",train_set="user_prod_train",test_set="user_prod_test",retrain=True,save_model=True):
    usercf_model=User_CF.User_CF(k_users,n_rec,weight_type,train_set,test_set,retrain,save_model)
    combine_matrix,user_prod_matrix_test,combine_var_cnt=usercf_model.preprocess()
    similarities_sparse,topn_neighbors=usercf_model.fit(combine_matrix)
    popular_items=usercf_model.find_popular_items()
    usercf_model.recommend_test(combine_matrix,user_prod_matrix_test,similarities_sparse,topn_neighbors,popular_items,combine_var_cnt)
    

def save_recommendation(table="user_prod",n_rec=20):
    user_prod=pd.DataFrame(utils.db_query("select VIP_cardnum,product_id,1 as value from " +table)) 
    user_prod.columns=["user_id","product_id","value"]
    user_prod_pivot=pd.pivot_table(user_prod,values='value', index='user_id', columns='product_id', fill_value=0)
    user_prod_pivot=user_prod_pivot.apply(lambda x:(x-np.min(x))/(np.max(x)-np.min(x)),axis=0)
    usercf_model=User_CF.User_CF(retrain=False)
    similarities_sparse,topn_neighbors=usercf_model.fit('')
    try:
        utils.db_query_exec("drop table saved_recommendation") 
    except:
        pass 
    utils.db_query_exec("create table saved_recommendation ('user_id' STRING, 'prod_id' STRING,'weight' FLOAT)")
    sql = "INSERT INTO saved_recommendation VALUES (%s, %s, %s)"
    #Only save users in similarities_sparse
    connection = mysql.connector.connect(host='127.0.0.1' ,   
                                        port='13301',
                                     database='bigobject',
                                     user='frank',password="frank")
    cursor = connection.cursor()
    users_to_recommend=list(set(user_prod_pivot.index) & set(topn_neighbors.index))
    user_rec_cnt=len(users_to_recommend)
    for user_id in users_to_recommend:
        rec=utils.recommend(user_prod_pivot,similarities_sparse,topn_neighbors,user_id,n_rec)
        val=[(user_id,rec.index[i],rec[i]) for i in range(len(rec))]
        cursor.executemany(sql, val)   
    cursor.close()
    return user_rec_cnt


'''
#Code for checking the recommendation manually by users 
Users=["B220251169","K221992176","Q224109908"]
def check_user_recommendation(Users):
    print("Examples:")
    for user_id in Users:
        try:
            print(user_id)
            print("buying record:")
            print(utils.db_query("select b.product_name from user_prod_train a inner join product b on a.product_id=b.product_id where VIP_cardnum='"+user_id+"'"))
            rec=utils.db_query("select b.product_name from saved_recommendation a inner join product b on a.prod_id=b.product_id where user_id='"+user_id+"'") 
            print("recommended:")
            print(rec)
        except:
            pass
            
check_user_recommendation(Users) 
'''

def main():
    k_users=50
    n_rec=10
    weight_type="frequency" 
    train_set="user_prod_train"
    test_set="user_prod_test" 
    retrain=True
    save_model=True
    if sys.argv[1]=="evaluate":
        print("=======================")
        run_evaluation_model(k_users=k_users,n_rec=n_rec,weight_type=weight_type,train_set=train_set,test_set=test_set,retrain=retrain,save_model=save_model)
    elif sys.argv[1]=="save_recommendation":
        user_rec_cnt=save_recommendation(table="user_prod",n_rec=n_rec)
        print("Amount of users recommended:{}".format(user_rec_cnt))
        total_users=utils.db_query("select * from total_users")
        print(total_users[0][0])
        percentage_users_rec=user_rec_cnt/total_users[0][0]*1.0
        print("Percentage of users recommended:",percentage_users_rec)
        
    elif sys.argv[1]=="recommend": #"F226972009"
        print(sys.argv[2])
        try:
            rec=utils.db_query("select prod_id from saved_recommendation where user_id='"+sys.argv[2]+"'") 
            print(rec)
        except:
            print("no recommendation")

if __name__=="__main__":
    main()

#改pivot table!!(user_prod_pivot in python) OK
#改可以filter bought items 
#print recommendation percentage count 
#unit check (OK) (filter out unecessary products) 
#lower the weight 
#1. change to scalable 
#python3 main.py "save_recommendation"-->slow
#run time ?