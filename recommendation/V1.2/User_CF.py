import utils
import pandas as pd
import numpy as np
from scipy import sparse
import pickle

class User_CF():
    """
    User-based collaberative filtering
    TopN recommendation
    """
    def __init__(self,k_users=20,n_rec=10,weight_type="frequency",train_set="user_prod_train",test_set="user_prod_test",retrain=True,save_model=False): 
        print("UserBasedCF start...\n")
        self.k_users = k_users
        self.n_rec = n_rec
        self.train_set=train_set
        self.test_set=test_set
        self.weight_type = weight_type 
        self.retrain=retrain
        self.save_model=save_model
        print("k_users:" +str(k_users))
        print("n_rec:" +str(n_rec))
        print("weight_type:" +str(weight_type))
        print("retrain:" +str(retrain))
        print("save_model:" +str(save_model))
        
    def other_info_to_combine(self):
        _,user_cat_matrix_train=utils.create_user_item_matrix(type="self_define")
        user_sex_train=pd.DataFrame(utils.db_query(sql_cmd="select VIP_cardnum,sex from user_sex_train"),columns=["user_id","sex"])
        user_sex_train['sex'].replace('','0',inplace=True)
        user_sex_train=user_sex_train.set_index("user_id").astype(float)
        user_cat_matrix_train=user_cat_matrix_train.merge(user_sex_train,on="user_id",how='left')
        return user_cat_matrix_train
        
    def find_popular_items(self):#,filename="popular_items.csv"
        popular_items=pd.DataFrame(utils.db_query("select product_id from popular_items")).values.flatten()
        #popular_items=pd.read_csv(filename,index_col=0,dtype='str').values.flatten()
        return popular_items

    def normalize_by_item(self,training_data):
        training_data_norm=training_data.apply(lambda x:(x-np.min(x))/(np.max(x)-np.min(x)),axis=0)
        return training_data_norm
        
    def create_cosine_similarity_matrix(self,user_prod_matrix_train_norm):
        user_prod_matrix_train_sparse=sparse.csr_matrix(user_prod_matrix_train_norm)
        similarities_sparse=utils.cosine_similarity(user_prod_matrix_train_sparse,dense_output=False)
        return similarities_sparse
    def preprocess(self):
        print("++Training++")
        _,user_prod_matrix_train=utils.create_user_item_matrix(table=self.train_set,type=self.weight_type)
        print(user_prod_matrix_train.shape)
        user_cat_matrix_train=self.other_info_to_combine()
        user_prod_matrix_train_norm=self.normalize_by_item(user_prod_matrix_train)
        user_cat_matrix_train_norm=self.normalize_by_item(user_cat_matrix_train)
        combine_var_cnt=user_cat_matrix_train_norm.shape[1]
        #combine 
        combine_matrix=user_prod_matrix_train_norm.merge(user_cat_matrix_train_norm,on="user_id",how='left').fillna(0)
        print('Training table shape(include other factors):'+str(combine_matrix.shape[0])+','+str(combine_matrix.shape[1]))
        print("++Testing++")
        _,user_prod_matrix_test=utils.create_user_item_matrix(table=self.test_set,type="onehot")
        print(user_prod_matrix_test.shape)
        user_prod_matrix_test=user_prod_matrix_test.loc[:,user_prod_matrix_test.columns.isin(user_prod_matrix_train.columns)]
        user_prod_matrix_test=user_prod_matrix_test.loc[user_prod_matrix_test.T.sum()!=0,:]
        return combine_matrix,user_prod_matrix_test,combine_var_cnt


    def fit(self,combine_matrix):     
        if self.retrain==True:
            print("start fitting")
            similarities_sparse=self.create_cosine_similarity_matrix(combine_matrix)
            topn_neighbors=utils.find_topn_neighbors(combine_matrix,similarities_sparse,n=self.k_users)
            if self.save_model==True:
                with open("similarity_sparse.pickle", "wb") as f:
                    pickle.dump(similarities_sparse, f)
                with open("topn_neighbors.pickle", "wb") as f:
                    pickle.dump(topn_neighbors, f)   
            return similarities_sparse,topn_neighbors
        else:
            print("start loading model")
            with open("similarity_sparse.pickle", 'rb') as f:
                similarities_sparse = pickle.load(f)
            with open("topn_neighbors.pickle", 'rb') as f:
                topn_neighbors = pickle.load(f)
            return similarities_sparse,topn_neighbors

    def recommend_test(self,train_data,test_data,similarities_sparse,topn_neighbors,popular_items,combine_var_cnt):
        """
        use testing data to evaluate and print out precision, recall, coverage and popularity  
        """
        utils.test_recommend_evaluation(train_data,test_data,similarities_sparse,topn_neighbors,popular_items,self.n_rec,combine_var_cnt)
    
    
        
