﻿import User_CF
import sys
import utils
import pandas as pd 
import numpy as np
import mysql.connector
from time import time 
import pickle

def run_evaluation_model(k_users=20,n_rec=10,weight_type="onehot",train_set="user_prod_train",test_set="user_prod_test",retrain=True,evaluate=True,cluster=True,User2clus_df=''):
    model_train_start=time()
    usercf_model=User_CF.User_CF(k_users,n_rec,weight_type,train_set,test_set,retrain,cluster)
    combine_matrix,user_prod_matrix_test,combine_var_cnt=usercf_model.preprocess()
    if cluster==True:
        User2clus_df,combine_matrix,user_prod_matrix_test=usercf_model.find_user_cluster(combine_matrix,user_prod_matrix_test,n_clusters=500)
        try:
            utils.db_query_exec("drop table User2clus_df") 
        except:
            pass 
        utils.db_query_exec("create table User2clus_df ('User_id' STRING, 'Cluster_num' STRING)")
        sql = "INSERT INTO User2clus_df VALUES (%s, %s)"
        connection = mysql.connector.connect(host=utils.host ,   
                                        port=utils.port,
                                     database=utils.database,
                                     user=utils.user ,
                                     password=utils.password)
        cursor = connection.cursor()
        val=[(row[0],row[1]) for i,row in User2clus_df.iterrows()]
        cursor.executemany(sql, val)    
        #combine_matrix.to_pickle("cluster_prod_mean.pkl") 
    #combine_matrix=combine_matrix.iloc[:100]    
    usercf_model.fit(combine_matrix)
    model_train_end=time()
    print("Similarity model train and save time:{} secs.".format(model_train_end-model_train_start))
                
    if evaluate==True:
        popular_items=usercf_model.find_popular_items()
        usercf_model.recommend_test(combine_matrix,user_prod_matrix_test,popular_items,combine_var_cnt,cluster,User2clus_df)
    else:
        pass

def save_recommendation(k_users=20,n_rec=10,weight_type="frequency",retrain=True,table="user_prod",cluster=True):
    user_prod=pd.DataFrame(utils.db_query("select VIP_cardnum,product_id,1 as value from " +table)) 
    user_prod.columns = ["user_id","product_id","value"]
    user_prod=pd.pivot_table(user_prod,values='value', index='user_id', columns='product_id', fill_value=0)
    user_prod=user_prod.apply(lambda x:(x-np.min(x))/(np.max(x)-np.min(x)),axis=0)
    usercf_model=User_CF.User_CF(k_users,n_rec,weight_type,retrain=retrain,cluster=cluster)     
    if cluster==True:
        User2clus_df=pd.DataFrame(utils.db_query("select * from User2clus_df")) 
        User2clus_df.columns=["User_id","Cluster_num"]
        #User2clus_df.loc[User2clus_df['User_id'].isin(user_prod.values)]
        user_prod=user_prod.merge(User2clus_df,left_on="user_id",right_on="User_id")
        users_to_recommend=user_prod.User_id #1079
        user_prod=user_prod.drop(["User_id"],axis=1)
        user_prod=user_prod.groupby('Cluster_num').mean()
    else:
        users_to_recommend=list(user_prod.index)
    try:
        utils.db_query_exec("drop table saved_recommendation") 
    except:
        pass     
    utils.db_query_exec("create table saved_recommendation ('user_id' STRING, 'prod_id' STRING,'weight' FLOAT)")
    sql = "INSERT INTO saved_recommendation VALUES (%s, %s, %s)"
    connection = mysql.connector.connect(host=utils.host ,   
                                        port=utils.port,
                                     database=utils.database,
                                     user=utils.user ,
                                     password=utils.password)
    
    cursor = connection.cursor()
    user_rec_cnt=0
    chunk=1000
    for idx in range(0, len(users_to_recommend), chunk):
        print(idx)
        temp_users=users_to_recommend[idx:idx+chunk]
        temp_users_str='\"'+'\",\"'.join([i for i in temp_users])+'\"'
        if cluster==True:
            user_sim_df=pd.DataFrame(utils.db_query("select b.User_id as Userid,CAST(topn_neighbor as INT16) as topn_neighbor,similarity from (select CAST(CAST(user_id AS INT32) AS STRING) as user_id,topn_neighbor,similarity from all_sim_df) a outer join User2clus_df b on a.user_id=b.Cluster_num where Userid in ("+temp_users_str+")")) 
        else:
            user_sim_df=pd.DataFrame(utils.db_query("select * from all_sim_df where user_id in ("+temp_users_str+")"))
        user_sim_df.columns=["Userid","topn_neighbor","similarity"]
        user_sim_df.set_index("Userid",inplace=True)
        if cluster==True:
            user_sim_df["topn_neighbor"]=user_sim_df["topn_neighbor"].astype(int).astype(str)
        user_sim_df=user_sim_df.merge(user_prod,left_on="topn_neighbor",right_index=True)
        user_sim_df=user_sim_df.iloc[:,2:].mul(user_sim_df["similarity"],axis=0)
        #user_sim_df=user_sim_df.apply(lambda x:x.iloc[2:]*x["similarity"],axis=1)
        for user_id in temp_users:
            try:
                #why many recommendation weight=0 
                rec=utils.recommend(user_prod,user_sim_df,user_id,n_rec)
                val=[(user_id,rec.index[i],rec[i]) for i in range(len(rec))]
                cursor.executemany(sql, val)
                user_rec_cnt+=1
            except:
                continue
    cursor.close()
    return user_rec_cnt

'''
#Code for checking the recommendation manually by users 
#指定userid, print出之前的購買紀錄和推薦商品
Users=["5805094580","A100398300","A100786060"]
def check_user_recommendation(Users):
    print("Examples:")
    for user_id in Users:
        try:
            print(user_id)
            print("buying record:")
            print(utils.db_query("select b.product_name from user_prod_train a inner join product b on a.product_id=b.product_id where VIP_cardnum='"+user_id+"'"))
            rec=utils.db_query("select b.product_name from saved_recommendation a inner join product b on a.prod_id=b.product_id where user_id='"+user_id+"'") 
            print("recommended:")
            print(rec)
        except:
            pass

check_user_recommendation(Users) 

'''

def main():
    #set parameters 
    k_users=30 #30 #use k neighbors records to build model
    n_rec=30 #recommend n items
    weight_type="onehot" #onehot(1:buy,0:not buy) or frequency(purchase frequency) 
    train_set="user_prod_train" #user purchase record used for training(bo table name) 
    test_set="user_prod_test" #user purchase record used for testing(bo table name) 
    #recommend_set="user_prod"
    retrain=True #True or False #whether to retrain or use previous trained model 
    evaluate=True #True or False #whether to evaluate the result (for testing data)
    cluster=True #whether to use k-means cluster instead of user record for training 

    if sys.argv[1]=="evaluate":
        print("Start training model")
        run_evaluation_model(k_users=k_users,n_rec=n_rec,weight_type=weight_type,train_set=train_set,test_set=test_set,retrain=retrain,evaluate=evaluate,cluster=cluster)
    elif sys.argv[1]=="save_recommendation":
        print("Start saving recommendation")
        user_rec_cnt=save_recommendation(k_users=k_users,n_rec=n_rec,weight_type=weight_type,retrain=retrain,table="user_prod",cluster=cluster)
        print("Amount of users recommended:{}".format(user_rec_cnt))
        total_users=utils.db_query("select * from total_users")
        #print(total_users[0][0])
        percentage_users_rec=user_rec_cnt/total_users[0][0]*1.0
        print("Percentage of users recommended:",percentage_users_rec)
    elif sys.argv[1]=="recommend":
        print(sys.argv[2])
        try:
            rec=utils.db_query("select prod_id from saved_recommendation where user_id='"+sys.argv[2]+"'") 
            print([i[0] for i in rec])
        except:
            print("no recommendation")

if __name__=="__main__":
    start=time()
    main()
    end=time()
    print("Total run time: "+str(end-start)+" secs.")
