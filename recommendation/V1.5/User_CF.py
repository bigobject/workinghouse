import utils
import pandas as pd
import numpy as np
from scipy import sparse
import pickle
import mysql.connector

class User_CF():
    """
    User-based collaberative filtering
    TopN recommendation
    """
    def __init__(self,k_users=20,n_rec=10,weight_type="frequency",train_set="user_prod_train",test_set="user_prod_test",retrain=True,cluster=True): 
        print("UserBasedCF start...\n")
        self.k_users = k_users
        self.n_rec = n_rec
        self.train_set=train_set
        self.test_set=test_set
        self.weight_type = weight_type 
        self.retrain=retrain
        self.cluster=cluster
        print("k_users:" +str(k_users))
        print("n_rec:" +str(n_rec))
        print("weight_type:" +str(weight_type))
        print("retrain:" +str(retrain))
        print("cluster:" +str(cluster))
        
    def other_info_to_combine(self):
        _,user_cat_matrix_train=utils.create_user_item_matrix(type="self_define")
        user_sex_train=pd.DataFrame(utils.db_query(sql_cmd="select VIP_cardnum,sex from user_sex_train"),columns=["user_id","sex"])
        user_sex_train['sex'].replace('','0',inplace=True)
        user_sex_train=user_sex_train.set_index("user_id").astype(float)
        user_cat_matrix_train=user_cat_matrix_train.merge(user_sex_train,on="user_id",how='left')
        return user_cat_matrix_train
        
    def find_popular_items(self):#,filename="popular_items.csv"
        popular_items=pd.DataFrame(utils.db_query("select product_id from popular_items")).values.flatten()
        #popular_items=pd.read_csv(filename,index_col=0,dtype='str').values.flatten()
        return popular_items
        
    def find_user_cluster(self,combine_matrix,user_prod_matrix_test,n_clusters=500):
        from sklearn.cluster import KMeans
        from collections import Counter
        kmeans = KMeans(
                init="random",
                n_clusters=n_clusters,
                n_init=10,
                max_iter=300,
                random_state=42
            ).fit(combine_matrix)
        
        #print("Each cluster user count:{}".format(Counter(kmeans.labels_)))
        User2clus_df=pd.DataFrame({"User_id":combine_matrix.index,"Cluster_num":kmeans.labels_})
        combine_matrix_cluster=pd.DataFrame(kmeans.cluster_centers_)
        combine_matrix_cluster.columns=combine_matrix.columns
        combine_matrix_cluster.index.name="cluster"
        user_prod_matrix_test_cluster=user_prod_matrix_test.merge(User2clus_df,left_index=True,right_on="User_id")
        #[['User_id','Cluster_num']].merge(combine_matrix_cluster,left_on="Cluster_num",right_index=True)
        return User2clus_df,combine_matrix_cluster,user_prod_matrix_test_cluster
    
    def normalize_by_item(self,training_data):
        training_data_norm=training_data.apply(lambda x:(x-np.min(x))/(np.max(x)-np.min(x)),axis=0)
        return training_data_norm
        

    def preprocess(self):
        print("++Training++")
        _,user_prod_matrix_train=utils.create_user_item_matrix(table=self.train_set,type=self.weight_type)
        print(user_prod_matrix_train.shape)
        use_other_factors=True
        user_cat_matrix_train=self.other_info_to_combine()
        user_prod_matrix_train_norm=self.normalize_by_item(user_prod_matrix_train)
        user_cat_matrix_train_norm=self.normalize_by_item(user_cat_matrix_train)
        #combine 
        if use_other_factors==True:
            combine_var_cnt=user_cat_matrix_train_norm.shape[1]
            combine_matrix=user_prod_matrix_train_norm.merge(user_cat_matrix_train_norm,on="user_id",how='left').fillna(0)
        else:
            combine_var_cnt=0
            combine_matrix=user_prod_matrix_train_norm.fillna(0)
        print('Training table shape(include other factors):'+str(combine_matrix.shape[0])+','+str(combine_matrix.shape[1]))
        print("++Testing++")
        _,user_prod_matrix_test=utils.create_user_item_matrix(table=self.test_set,type="onehot")
        print(user_prod_matrix_test.shape)
        user_prod_matrix_test=user_prod_matrix_test.loc[:,user_prod_matrix_test.columns.isin(user_prod_matrix_train.columns)]
        user_prod_matrix_test=user_prod_matrix_test.loc[user_prod_matrix_test.T.sum()!=0,:]
        return combine_matrix,user_prod_matrix_test,combine_var_cnt
        
    def fit(self,combine_matrix):     
        if self.retrain==True:
            print("start fitting")
            try:
                utils.db_query_exec("create table all_sim_df ('user_id' STRING, 'topn_neighbor' STRING,'similarity' FLOAT)")
            except:
                utils.db_query_exec("drop table all_sim_df") 
                utils.db_query_exec("create table all_sim_df ('user_id' STRING, 'topn_neighbor' STRING,'similarity' FLOAT)")
            utils.create_similarity_df(combine_matrix,k_users=self.k_users)
#train_data=combine_matrix
#test_data=user_prod_matrix_test
    def recommend_test(self,train_data,test_data,popular_items,combine_var_cnt,cluster,User2clus_df):
        precision_all={}
        recall_all={}
        all_test_recommend=set()
        popular_sum=0
        rec_count=0
        if combine_var_cnt!=0:
            train_data=train_data.iloc[:,:-combine_var_cnt]        
        chunk=1000
        if cluster==True:
            test_data_filter=test_data[test_data['Cluster_num'].isin(train_data.index)]
            test_data_filter=test_data_filter.set_index("User_id").drop("Cluster_num",axis=1)
            all_users=test_data['User_id'].unique()
            print('number of testing user recommended:{}'.format(len(all_users)))
        else:
            test_data_filter=test_data[test_data.index.isin(train_data.index)]
            all_users=test_data_filter.index.unique().values
            print('number of testing user recommended:{}'.format(len(all_users)))
    
        for idx in range(0, len(all_users), chunk):
            print(idx)
            if cluster==True:
                temp_users=all_users[idx:idx+chunk]
                user_sim_df=pd.DataFrame(utils.db_query("select * from all_sim_df "))
                user_sim_df.columns=["Userid","topn_neighbor","similarity"]
                user_sim_df["topn_neighbor"]=user_sim_df["topn_neighbor"].astype(float).astype('int32')
                user_sim_df["Userid"]=user_sim_df["Userid"].astype(float).astype('int32')
                user_sim_df=User2clus_df.loc[User2clus_df['User_id'].isin(temp_users)].merge(user_sim_df,left_on="Cluster_num",right_on="Userid").drop(['Cluster_num','Userid'],axis=1)
                user_sim_df.set_index("User_id",inplace=True)
            else:
                temp_users=all_users[idx:idx+chunk]
                temp_users_str='\"'+'\",\"'.join([i for i in temp_users])+'\"'
                user_sim_df=pd.DataFrame(utils.db_query("select * from all_sim_df where user_id in ("+temp_users_str+")"))
                user_sim_df.columns=["Userid","topn_neighbor","similarity"]
                user_sim_df.set_index("Userid",inplace=True)            
            user_sim_df=user_sim_df.merge(train_data,left_on="topn_neighbor",right_index=True)
            #each user should have 10 neighbors 
            user_sim_df=user_sim_df.iloc[:,2:].mul(user_sim_df["similarity"],axis=0)
            for user_id in temp_users:
                test=test_data_filter.loc[test_data_filter.index==user_id].apply(lambda x:set(x[x>0].index.values),axis=1).values[0]
                test_recommend=set(utils.recommend(train_data,user_sim_df,user_id,self.n_rec).index.values)
                for i in test_recommend:
                    all_test_recommend.add(i)
                    if i in popular_items:
                        popular_sum+=1
                    rec_count+=1
                hit=len(test.intersection(test_recommend))
                real_purchase=len(test)            
                precision_all[user_id]=hit/self.n_rec*1.0
                recall_all[user_id]=hit/real_purchase*1.0
        mean_precision=np.mean(list(precision_all.values()))
        mean_recall=np.mean(list(recall_all.values()))
        coverage = len(all_test_recommend) / (1.0 * train_data.shape[1])   
        popularity = popular_sum / (self.n_rec*len(all_users)*1.0)
        print("Precision:"+str(mean_precision))
        print("Recall:"+str(mean_recall))
        print("Coverage:"+str(coverage))
        print("Popularity:"+str(popularity)) 

