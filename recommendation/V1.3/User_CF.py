import utils
import pandas as pd
import numpy as np
from scipy import sparse
import pickle
import mysql.connector

class User_CF():
    """
    User-based collaberative filtering
    TopN recommendation
    """
    def __init__(self,k_users=20,n_rec=10,weight_type="frequency",train_set="user_prod_train",test_set="user_prod_test",retrain=True,save_model=False): 
        print("UserBasedCF start...\n")
        self.k_users = k_users
        self.n_rec = n_rec
        self.train_set=train_set
        self.test_set=test_set
        self.weight_type = weight_type 
        self.retrain=retrain
        self.save_model=save_model
        print("k_users:" +str(k_users))
        print("n_rec:" +str(n_rec))
        print("weight_type:" +str(weight_type))
        print("retrain:" +str(retrain))
        print("save_model:" +str(save_model))
        
    def other_info_to_combine(self):
        _,user_cat_matrix_train=utils.create_user_item_matrix(type="self_define")
        user_sex_train=pd.DataFrame(utils.db_query(sql_cmd="select VIP_cardnum,sex from user_sex_train"),columns=["user_id","sex"])
        user_sex_train['sex'].replace('','0',inplace=True)
        user_sex_train=user_sex_train.set_index("user_id").astype(float)
        user_cat_matrix_train=user_cat_matrix_train.merge(user_sex_train,on="user_id",how='left')
        return user_cat_matrix_train
        
    def find_popular_items(self):#,filename="popular_items.csv"
        popular_items=pd.DataFrame(utils.db_query("select product_id from popular_items")).values.flatten()
        #popular_items=pd.read_csv(filename,index_col=0,dtype='str').values.flatten()
        return popular_items

    def normalize_by_item(self,training_data):
        training_data_norm=training_data.apply(lambda x:(x-np.min(x))/(np.max(x)-np.min(x)),axis=0)
        return training_data_norm
        

    def preprocess(self):
        print("++Training++")
        _,user_prod_matrix_train=utils.create_user_item_matrix(table=self.train_set,type=self.weight_type)
        print(user_prod_matrix_train.shape)
        user_cat_matrix_train=self.other_info_to_combine()
        user_prod_matrix_train_norm=self.normalize_by_item(user_prod_matrix_train)
        user_cat_matrix_train_norm=self.normalize_by_item(user_cat_matrix_train)
        combine_var_cnt=user_cat_matrix_train_norm.shape[1]
        #combine 
        combine_matrix=user_prod_matrix_train_norm.merge(user_cat_matrix_train_norm,on="user_id",how='left').fillna(0)
        print("sparcity:",np.sum(combine_matrix==0).sum()/np.prod(combine_matrix.shape))
        print('Training table shape(include other factors):'+str(combine_matrix.shape[0])+','+str(combine_matrix.shape[1]))
        print("++Testing++")
        _,user_prod_matrix_test=utils.create_user_item_matrix(table=self.test_set,type="onehot")
        print(user_prod_matrix_test.shape)
        user_prod_matrix_test=user_prod_matrix_test.loc[:,user_prod_matrix_test.columns.isin(user_prod_matrix_train.columns)]
        user_prod_matrix_test=user_prod_matrix_test.loc[user_prod_matrix_test.T.sum()!=0,:]
        return combine_matrix,user_prod_matrix_test,combine_var_cnt
        
    #def fit(self,combine_matrix):     
    #    if self.retrain==True:
    #        print("start fitting")
    #        all_sim_df=utils.create_similarity_df(combine_matrix,k_users=self.k_users)
    #        if self.save_model==True:
    #            with open("all_sim_df.pickle", "wb") as f:
    #                pickle.dump(all_sim_df, f)
    #        return all_sim_df
    #    else:
    #        print("start loading model")
    #        with open("all_sim_df.pickle", 'rb') as f:
    #            all_sim_df = pickle.load(f)
    #        return all_sim_df
    def fit(self,combine_matrix):     
        if self.retrain==True:
            print("start fitting")
            try:
                utils.db_query_exec("create table all_sim_df ('user_id' STRING, 'topn_neighbor' STRING,'similarity' FLOAT)")
            except:
                utils.db_query_exec("drop table all_sim_df") 
                utils.db_query_exec("create table all_sim_df ('user_id' STRING, 'topn_neighbor' STRING,'similarity' FLOAT)")
            #split into 10 fold? combine_matrix.index[] 
            #N_folds = 10
            # Determine the correct indices to split the data.
            #limits = np.linspace(0, num_rows+1, N_fold+1, dtype=int)
            all_sim_df=utils.create_similarity_df(combine_matrix,k_users=self.k_users)
            if self.save_model==True:
                sql = "INSERT INTO all_sim_df VALUES (%s, %s, %s)"
                val = [tuple([x[2],x[0],x[1]]) for x in all_sim_df.to_numpy()]
                connection = mysql.connector.connect(host=utils.            host ,
                port=utils.port,
                database=utils.database,
                user=utils.user ,
                password=utils.password)
                cursor = connection.cursor()
                cursor.executemany(sql, val)
            return all_sim_df
        else:
            print("start loading model")
            all_sim_df=utils.db_query("select * from all_sim_df")
            all_sim_df=pd.DataFrame(all_sim_df)
            all_sim_df.columns=['Userid','topn_neighbor','similarity']
            return all_sim_df

    def recommend_test(self,train_data,test_data,all_sim_df,popular_items,combine_var_cnt):
        #only take user that are in training
        test_data_filter=test_data[test_data.index.isin(train_data.index)]
        all_users=test_data_filter.index.unique().values
        print('number of testing user recommended:{}'.format(len(all_users)))
        precision_all={}
        recall_all={}
        all_test_recommend=set()
        popular_sum=0
        rec_count=0
        train_data=train_data.iloc[:,:-combine_var_cnt]
        all_sim_df.set_index("Userid",inplace=True)
        all_sim_df=all_sim_df.merge(train_data,left_on="topn_neighbor",right_index=True)
        for user_id in all_users:
            #print(user_id)
            test=test_data_filter.loc[test_data_filter.index==user_id].apply(lambda x:set(x[x>0].index.values),axis=1).values[0]
            test_recommend=set(utils.recommend(train_data,all_sim_df,user_id,self.n_rec).index.values)
            for i in test_recommend:
                all_test_recommend.add(i)
                if i in popular_items:
                    popular_sum+=1
                rec_count+=1
            hit=len(test.intersection(test_recommend))
            real_purchase=len(test)            
            precision_all[user_id]=hit/self.n_rec*1.0
            recall_all[user_id]=hit/real_purchase*1.0
    
        mean_precision=np.mean(list(precision_all.values()))
        mean_recall=np.mean(list(recall_all.values()))
        coverage = len(all_test_recommend) / (1.0 * train_data.shape[1])   
        popularity = popular_sum / (self.n_rec*len(all_users)*1.0)
        print("Precision:"+str(mean_precision))
        print("Recall:"+str(mean_recall))
        print("Coverage:"+str(coverage))
        print("Popularity:"+str(popularity))    
              
